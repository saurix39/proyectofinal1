<?php
class ClienteDAO{
    private $idCliente;
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $direccion;
    private $foto;
    private $estado;

    public function ClienteDAO($idCliente="",$id="",$nombre="",$apellido="",$correo="",$clave="",$direccion="",$foto="",$estado=""){
        $this -> idCliente = $idCliente;
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> direccion = $direccion;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }

    public function autenticar(){
        return "select idcliente, estado from cliente where correo='". $this -> correo ."' and clave='". md5($this -> clave) ."'";
    }
    public function consultar(){
        return "select id, nombre, apellido, correo, foto, direccion from cliente where idcliente='". $this -> idCliente ."'";
    }

    public function existeCorreo(){
        return "select correo from cliente where correo='". $this -> correo ."'";
    }

    public function registrarCliente(){
        return "insert into cliente (correo, clave, estado) values ('". $this -> correo ."', '". md5($this -> clave) ."', 1)"; 
    }

    public function lista($cantidad,$pagina){
        return "select idcliente,id,nombre,apellido,correo,direccion,estado
                from cliente
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idcliente) from cliente";
    }

    public function habilitar(){
        return "update cliente set estado=1
                where idcliente='". $this -> idCliente ."'";
    }

    public function inhabilitar(){
        return "update cliente set estado=0
                where idcliente='". $this -> idCliente ."'";
    }

    public function consultarEstado(){
        return "select estado from cliente where idcliente='". $this -> idCliente ."'";
    }

    public function actualizarInformacion(){
        return "update cliente set id='". $this -> id ."', nombre='". $this -> nombre ."', apellido='". $this -> apellido ."', direccion='". $this -> direccion ."', foto='". $this -> foto ."'  where idcliente='". $this -> idCliente ."'";
    }
}
?>