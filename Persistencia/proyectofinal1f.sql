-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-07-2020 a las 18:50:11
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectofinal1`
--
CREATE DATABASE IF NOT EXISTS `proyectofinal1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `proyectofinal1`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acciones`
--

CREATE TABLE `acciones` (
  `idaccion` bigint(50) NOT NULL,
  `nombreaccion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `acciones`
--

INSERT INTO `acciones` (`idaccion`, `nombreaccion`) VALUES
(1, 'Registro'),
(2, 'Ingreso'),
(3, 'Venta'),
(4, 'Cambio de estado'),
(5, 'Edicion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idadministrador` int(20) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idadministrador`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(1, 'Carlos', 'Martinez', '123@123.com', '202cb962ac59075b964b07152d234b70', NULL, 1),
(3, 'Marcos', 'Ferreira', '906@906.com', 'c8fbbc86abe8bd6a5eb6a3b4d0411301', 'Imagenes/Administrador/1593838377.png', 1),
(4, 'Francisco', 'Martinez', '444@444.com', 'dbc4d84bfcfe2284ba11beffb853a8c4', 'Imagenes/Administrador/1594570019.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` bigint(50) NOT NULL,
  `id` int(25) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `id`, `nombre`, `apellido`, `correo`, `clave`, `direccion`, `estado`, `foto`) VALUES
(10000000, 4522689, 'James', 'Rodriguez', '100@100.com', '202cb962ac59075b964b07152d234b70', 'Cll41C#12mNor', 1, 'Imagenes/Cliente/1594395680.jpg'),
(10000003, 326568, 'Andres', 'Colinas', '300@300.com', '94f6d7e04a4d452035300f18b984988c', 'tv5411255', 1, 'Imagenes/Cliente/1594831667.jpg'),
(10000004, 1115, 'javier', 'perez', '400@400.com', '400', NULL, 1, NULL),
(10000005, 5656, 'Marcos', 'Rojo', '500@500.com', '500', NULL, 1, NULL),
(10000006, 225, 'Pablo', 'Mojica', '600@600.com', '600', NULL, 1, NULL),
(10000007, 63225636, 'Mario', 'Bros', '307@307.com', '8e98d81f8217304975ccb23337bb5761', 'tv44b#10cSur', 1, 'Imagenes/Cliente/1594757064.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logadministrador`
--

CREATE TABLE `logadministrador` (
  `idlog` bigint(20) NOT NULL,
  `idaccion_FK` bigint(50) NOT NULL,
  `idadministrador_FK` int(20) NOT NULL,
  `informacion` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `logadministrador`
--

INSERT INTO `logadministrador` (`idlog`, `idaccion_FK`, `idadministrador_FK`, `informacion`, `fecha_hora`) VALUES
(1, 2, 1, 'Ingreso el siguiente administrador:<br>\r\nidentificación: 1<br>\r\ncorreo: 123@123.com', '2020-07-12 09:28:52'),
(2, 2, 1, 'Ingreso el siguiente administrador:<br>\r\nidentificación: 1<br>\r\ncorreo: 123@123.com', '2020-07-12 22:56:38'),
(3, 4, 1, 'Se actualizo el estado del vehiculo:<br>\r\nidentificación: 1000008<br>\r\nadministrador: 1<br>\r\nEstado anterior: Habilitado<br>\r\nEstado posterior: Inhabilitado', '2020-07-12 22:56:57'),
(4, 4, 1, 'Se actualizo el estado del vehiculo:<br>\r\nidentificación: 1000008<br>\r\nadministrador: 1<br>\r\nEstado anterior: Inhabilitado<br>\r\nEstado posterior: Habilitado', '2020-07-12 22:57:28'),
(5, 4, 1, 'Se actualizo el estado del cliente:<br>\r\nidentificación: 10000007<br>\r\nadministrador: 1<br>\r\nEstado anterior: Habilitado<br>\r\nEstado posterior: Inhabilitado', '2020-07-12 22:58:26'),
(6, 4, 1, 'Se actualizo el estado del cliente:<br>\r\nidentificación: 10000007<br>\r\nadministrador: 1<br>\r\nEstado anterior: Inhabilitado<br>\r\nEstado posterior: Habilitado', '2020-07-12 22:58:45'),
(7, 4, 1, 'Se actualizo el estado del Administrador:<br>\r\nidentificación: 3<br>\r\nadministrador: 1<br>\r\nEstado anterior: Habilitado<br>\r\nEstado posterior: Inhabilitado', '2020-07-12 22:59:34'),
(8, 4, 1, 'Se actualizo el estado del Administrador:<br>\r\nidentificación: 3<br>\r\nadministrador: 1<br>\r\nEstado anterior: Inhabilitado<br>\r\nEstado posterior: Habilitado', '2020-07-12 22:59:44'),
(9, 4, 1, 'Se actualizo el estado del Proveedor:<br>\r\nidentificación: 1006<br>\r\nadministrador: 1<br>\r\nEstado anterior: Habilitado<br>\r\nEstado posterior: Inhabilitado', '2020-07-12 23:00:16'),
(10, 4, 1, 'Se actualizo el estado del Proveedor:<br>\r\nidentificación: 1006<br>\r\nadministrador: 1<br>\r\nEstado anterior: Inhabilitado<br>\r\nEstado posterior: Habilitado', '2020-07-12 23:00:35'),
(11, 1, 1, 'Se registro un Proveedor con la siguiente información:<br>\r\nidentificación: 1007<br>\r\nnombre: McLaren<br>\r\ncorreo: 999@999.com<br>\r\nnit: 326522365', '2020-02-12 20:31:19'),
(12, 1, 1, 'Se registro un Administrador con la siguiente información:<br>\r\nidentificación: 4<br>\r\nnombre: Francisco<br>\r\napellido: Martinez<br>\r\ncorreo: 444@444.com', '2020-06-12 23:07:00'),
(13, 2, 4, 'Ingreso el siguiente administrador:<br>\r\nidentificación: 4<br>\r\ncorreo: 444@444.com', '2020-07-12 23:08:24'),
(14, 2, 1, 'Ingreso el siguiente administrador:<br>\r\nidentificación: 1<br>\r\ncorreo: 123@123.com', '2020-07-12 23:54:25'),
(15, 2, 1, 'Ingreso el siguiente administrador:<br>\r\nidentificación: 1<br>\r\ncorreo: 123@123.com', '2020-07-12 23:54:43'),
(16, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-13 03:08:57'),
(17, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-13 04:20:24'),
(18, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-12 21:26:54'),
(19, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-13 00:16:42'),
(20, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-13 00:41:47'),
(21, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-13 15:32:41'),
(22, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-13 20:37:26'),
(23, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-13 20:40:52'),
(24, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 00:48:15'),
(25, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 00:54:33'),
(26, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 00:54:36'),
(27, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 00:54:38'),
(28, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 00:54:41'),
(29, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 00:54:49'),
(30, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000003<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 00:54:51'),
(31, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000003<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 00:54:52'),
(32, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 00:54:57'),
(33, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000007<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 00:54:58'),
(34, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000007<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 00:55:02'),
(35, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000006<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 00:55:06'),
(36, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000006<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 00:55:12'),
(37, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 02:45:27'),
(38, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:45:32'),
(39, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:45:35'),
(40, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:45:37'),
(41, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:45:42'),
(42, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000007<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:45:46'),
(43, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000007<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:45:50'),
(44, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1001<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:46:01'),
(45, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1001<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:46:02'),
(46, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1007<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:46:08'),
(47, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1007<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:46:09'),
(48, 4, 1, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:46:26'),
(49, 4, 1, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:46:27'),
(50, 4, 1, 'Se actualizo el estado del vehiculo:<br>identificación: 1000008<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:46:32'),
(51, 4, 1, 'Se actualizo el estado del vehiculo:<br>identificación: 1000008<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:46:33'),
(52, 4, 1, 'Se actualizo el estado del Administrador:<br>identificación: 4<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:46:47'),
(53, 4, 1, 'Se actualizo el estado del Administrador:<br>identificación: 4<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:46:50'),
(54, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 14:26:28'),
(55, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 14:26:56'),
(56, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 14:26:56'),
(57, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 14:45:08'),
(58, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 14:56:00'),
(59, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 14:56:12'),
(60, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 14:56:13'),
(61, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1002<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 14:56:40'),
(62, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1002<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 14:56:40'),
(63, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 19:07:06'),
(64, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 20:02:57'),
(65, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 20:05:34'),
(66, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 20:47:48'),
(67, 1, 1, 'Se registro un Proveedor con la siguiente información:<br>identificación: 1008<br>nombre: Audi<br>correo: au@au.com<br>nit: 23652589', '2020-07-14 20:48:40'),
(68, 1, 1, 'Se registro un Proveedor con la siguiente información:<br>identificación: 1009<br>nombre: Ferrari<br>correo: fe@fe.com<br>nit: 6262664', '2020-07-14 20:49:27'),
(69, 1, 1, 'Se registro un Proveedor con la siguiente información:<br>identificación: 1010<br>nombre: Toyota<br>correo: toy@toy.com<br>nit: 62626556', '2020-07-14 20:54:27'),
(70, 1, 1, 'Se registro un Proveedor con la siguiente información:<br>identificación: 1011<br>nombre: Jeep<br>correo: je@je.com<br>nit: 455522', '2020-07-14 20:58:58'),
(71, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1004<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 21:04:21'),
(72, 4, 1, 'Se actualizo el estado del Proveedor:<br>identificación: 1003<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 21:06:00'),
(73, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 21:48:57'),
(74, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 22:06:03'),
(75, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 22:34:46'),
(76, 4, 1, 'Se actualizo el estado del Administrador:<br>identificación: 1<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 22:36:50'),
(77, 4, 1, 'Se actualizo el estado del Administrador:<br>identificación: 1<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 22:36:52'),
(78, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-14 22:41:48'),
(79, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-15 01:02:17'),
(80, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-15 01:02:33'),
(81, 4, 1, 'Se actualizo el estado del cliente:<br>identificación: 10000000<br>administrador: 1<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-15 01:02:34'),
(82, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-15 01:41:32'),
(83, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-15 02:31:32'),
(84, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-15 16:28:32'),
(85, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-15 16:44:18'),
(86, 2, 1, 'Ingreso el siguiente administrador:<br>identificación: 1<br>correo: 123@123.com', '2020-07-15 16:48:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logcliente`
--

CREATE TABLE `logcliente` (
  `idlog` bigint(20) NOT NULL,
  `idaccion_FK` bigint(50) NOT NULL,
  `idcliente_FK` bigint(50) NOT NULL,
  `informacion` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `logcliente`
--

INSERT INTO `logcliente` (`idlog`, `idaccion_FK`, `idcliente_FK`, `informacion`, `fecha_hora`) VALUES
(1, 1, 10000007, 'Se registro un cliente con la siguiente información:<br>\r\ncorreo: 307@307.com<br>\r\nIdentificación: 10000007', '2020-07-12 06:49:04'),
(2, 2, 10000000, 'Ingreso el siguiente cliente:<br>\r\nidentificación: 10000000<br>\r\ncorreo: 100@100.com', '2020-07-12 09:27:24'),
(3, 3, 10000000, 'Se realizo la siguiente venta:<br>\r\nidentificación: 100000032<br>\r\ncliente: 10000000<br>\r\nValor: 41000000', '2020-07-12 02:27:56'),
(4, 2, 10000000, 'Ingreso el siguiente cliente:<br>\r\nidentificación: 10000000<br>\r\ncorreo: 100@100.com<br>', '2020-07-12 23:08:55'),
(5, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-13 17:10:54'),
(6, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-13 20:42:32'),
(7, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 00:09:22'),
(8, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 14:19:55'),
(9, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 14:29:10'),
(10, 3, 10000000, 'Se realizo la siguiente venta:<br>identificación: 100000033<br>cliente: 10000000<br>Valor: 41000000', '2020-07-14 14:31:25'),
(11, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 14:34:44'),
(12, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 14:35:43'),
(13, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 15:00:55'),
(14, 2, 10000007, 'Ingreso el siguiente cliente:<br>identificación: 10000007<br>correo: 307@307.com', '2020-07-14 20:03:26'),
(15, 3, 10000007, 'Se realizo la siguiente venta:<br>identificación: 100000034<br>cliente: 10000007<br>Valor: 291000000', '2020-07-14 20:04:56'),
(16, 3, 10000007, 'Se realizo la siguiente venta:<br>identificación: 100000035<br>cliente: 10000007<br>Valor: 41000000', '2020-07-14 20:05:26'),
(17, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 21:12:29'),
(18, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 21:48:37'),
(19, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 21:49:05'),
(20, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 22:09:49'),
(21, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 22:40:06'),
(22, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-14 22:46:11'),
(23, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-15 01:05:23'),
(24, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-15 01:07:58'),
(25, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-15 01:55:05'),
(26, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-15 02:23:48'),
(27, 2, 10000000, 'Ingreso el siguiente cliente:<br>identificación: 10000000<br>correo: 100@100.com', '2020-07-15 16:09:23'),
(28, 2, 10000003, 'Ingreso el siguiente cliente:<br>identificación: 10000003<br>correo: 300@300.com', '2020-07-15 16:45:35'),
(29, 5, 10000003, 'se edito el siguiente cliente:<br>identificacion: 10000003<br>Se ingresaron los siguientes datos:<br>cedula: 326568<br>nombre: <br>apellido: Colinas<br>direccion: tv5411255', '2020-07-15 16:46:55'),
(30, 5, 10000003, 'se edito el siguiente cliente:<br>identificacion: 10000003<br>Se ingresaron los siguientes datos:<br>cedula: 326568<br>nombre: Andres<br>apellido: Colinas<br>direccion: tv5411255', '2020-07-15 16:47:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logproveedor`
--

CREATE TABLE `logproveedor` (
  `idlog` bigint(20) NOT NULL,
  `idaccion_FK` bigint(50) NOT NULL,
  `idproveedor_FK` int(35) NOT NULL,
  `informacion` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `fecha_hora` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `logproveedor`
--

INSERT INTO `logproveedor` (`idlog`, `idaccion_FK`, `idproveedor_FK`, `informacion`, `fecha_hora`) VALUES
(1, 2, 1001, 'Ingreso el siguiente proveedor:<br>\r\nidentificación: 1001<br>\r\ncorreo: 200@200.com<br>', '2020-07-12 22:51:23'),
(2, 1, 1001, 'Se ingreso el nombre de vehiculo:<br>\r\nnombre: X6', '2020-07-12 22:51:32'),
(3, 1, 1001, 'se registro el siguiente vehiculo:<br>\r\nidentificación de fabrica: 25125<br>\r\nidentificación del proveedor: 1001<br>\r\nidentificación del nombre: 100007<br>\r\nmodelo: 2020<br>\r\nprecio: 250000000<br>', '2020-07-12 22:53:57'),
(4, 4, 1001, 'Se actualizo el estado del vehiculo:<br>\r\nidentificación: 1000008<br>\r\nproveedor: 1001<br>\r\nEstado anterior: Habilitado<br>\r\nEstado posterior: Inhabilitado', '2020-07-12 22:55:00'),
(5, 4, 1001, 'Se actualizo el estado del vehiculo:<br>\r\nidentificación: 1000008<br>\r\nproveedor: 1001<br>\r\nEstado anterior: Inhabilitado<br>\r\nEstado posterior: Habilitado', '2020-07-12 22:55:37'),
(6, 2, 1007, 'Ingreso el siguiente proveedor:<br>\r\nidentificación: 1007<br>\r\ncorreo: 999@999.com', '2020-07-12 23:54:34'),
(7, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 02:39:27'),
(8, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 02:51:42'),
(9, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>proveedor: 1001<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:51:48'),
(10, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>proveedor: 1001<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:51:49'),
(11, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000008<br>proveedor: 1001<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 02:51:54'),
(12, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000008<br>proveedor: 1001<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 02:52:23'),
(13, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 14:32:22'),
(14, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 14:35:00'),
(15, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 14:59:38'),
(16, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>proveedor: 1001<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-14 15:00:36'),
(17, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>proveedor: 1001<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-14 15:00:36'),
(18, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 21:00:32'),
(19, 1, 1001, 'Se ingreso el nombre de vehiculo:<br>nombre: Serie 7', '2020-07-14 21:00:40'),
(20, 1, 1001, 'Se ingreso el nombre de vehiculo:<br>nombre: M2', '2020-07-14 21:00:47'),
(21, 1, 1001, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 3233256<br>identificación del proveedor: 1001<br>identificación del nombre: 100008<br>modelo: 2020<br>precio: 170000000', '2020-07-14 21:01:39'),
(22, 1, 1001, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 225151<br>identificación del proveedor: 1001<br>identificación del nombre: 100009<br>modelo: 2020<br>precio: 250000000', '2020-07-14 21:02:03'),
(23, 2, 1002, 'Ingreso el siguiente proveedor:<br>identificación: 1002<br>correo: 400@400.com', '2020-07-14 21:02:40'),
(24, 1, 1002, 'Se ingreso el nombre de vehiculo:<br>nombre: Amg', '2020-07-14 21:02:48'),
(25, 1, 1002, 'Se ingreso el nombre de vehiculo:<br>nombre: Clase g', '2020-07-14 21:02:56'),
(26, 1, 1002, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 15152<br>identificación del proveedor: 1002<br>identificación del nombre: 100010<br>modelo: 2020<br>precio: 250000000', '2020-07-14 21:03:30'),
(27, 1, 1002, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 233656<br>identificación del proveedor: 1002<br>identificación del nombre: 100011<br>modelo: 2020<br>precio: 750000000', '2020-07-14 21:04:01'),
(28, 2, 1004, 'Ingreso el siguiente proveedor:<br>identificación: 1004<br>correo: 901@901.com', '2020-07-14 21:04:33'),
(29, 1, 1004, 'Se ingreso el nombre de vehiculo:<br>nombre: Aventator', '2020-07-14 21:04:50'),
(30, 1, 1004, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 6226<br>identificación del proveedor: 1004<br>identificación del nombre: 100012<br>modelo: 2020<br>precio: 2700000000', '2020-07-14 21:05:14'),
(31, 2, 1003, 'Ingreso el siguiente proveedor:<br>identificación: 1003<br>correo: 900@900.com', '2020-07-14 21:06:05'),
(32, 1, 1003, 'Se ingreso el nombre de vehiculo:<br>nombre: Camaro', '2020-07-14 21:06:11'),
(33, 1, 1003, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 262666<br>identificación del proveedor: 1003<br>identificación del nombre: 100013<br>modelo: 2020<br>precio: 1500000000', '2020-07-14 21:06:30'),
(34, 2, 1009, 'Ingreso el siguiente proveedor:<br>identificación: 1009<br>correo: fe@fe.com', '2020-07-14 21:06:55'),
(35, 1, 1009, 'Se ingreso el nombre de vehiculo:<br>nombre: 458i', '2020-07-14 21:07:06'),
(36, 1, 1009, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 565112<br>identificación del proveedor: 1009<br>identificación del nombre: 100014<br>modelo: 2020<br>precio: 2700000000', '2020-07-14 21:07:33'),
(37, 2, 1008, 'Ingreso el siguiente proveedor:<br>identificación: 1008<br>correo: au@au.com', '2020-07-14 21:08:15'),
(38, 1, 1008, 'Se ingreso el nombre de vehiculo:<br>nombre: Rs 6', '2020-07-14 21:08:29'),
(39, 1, 1008, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 626226<br>identificación del proveedor: 1008<br>identificación del nombre: 100015<br>modelo: 2020<br>precio: 500000000', '2020-07-14 21:08:52'),
(40, 1, 1008, 'Se ingreso el nombre de vehiculo:<br>nombre: R 8', '2020-07-14 21:08:59'),
(41, 1, 1008, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 241523<br>identificación del proveedor: 1008<br>identificación del nombre: 100016<br>modelo: 2020<br>precio: 350000000', '2020-07-14 21:09:21'),
(42, 2, 1010, 'Ingreso el siguiente proveedor:<br>identificación: 1010<br>correo: toy@toy.com', '2020-07-14 21:09:41'),
(43, 1, 1010, 'Se ingreso el nombre de vehiculo:<br>nombre: Supra', '2020-07-14 21:09:52'),
(44, 1, 1010, 'Se ingreso el nombre de vehiculo:<br>nombre: Tacoma', '2020-07-14 21:10:02'),
(45, 1, 1010, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 215132<br>identificación del proveedor: 1010<br>identificación del nombre: 100017<br>modelo: 2020<br>precio: 700000000', '2020-07-14 21:10:19'),
(46, 1, 1010, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 65656<br>identificación del proveedor: 1010<br>identificación del nombre: 100018<br>modelo: 2020<br>precio: 700000000', '2020-07-14 21:10:37'),
(47, 2, 1011, 'Ingreso el siguiente proveedor:<br>identificación: 1011<br>correo: je@je.com', '2020-07-14 21:10:51'),
(48, 1, 1011, 'Se ingreso el nombre de vehiculo:<br>nombre: Grand Cherokee', '2020-07-14 21:11:14'),
(49, 1, 1011, 'se registro el siguiente vehiculo:<br>identificación de fabrica: 626262<br>identificación del proveedor: 1011<br>identificación del nombre: 100019<br>modelo: 2020<br>precio: 350000000', '2020-07-14 21:11:52'),
(50, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 22:08:39'),
(51, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-14 22:47:38'),
(52, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-15 01:07:03'),
(53, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-15 01:56:03'),
(54, 2, 1001, 'Ingreso el siguiente proveedor:<br>identificación: 1001<br>correo: 200@200.com', '2020-07-15 15:32:50'),
(55, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>proveedor: 1001<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado', '2020-07-15 15:34:29'),
(56, 4, 1001, 'Se actualizo el estado del vehiculo:<br>identificación: 1000000<br>proveedor: 1001<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado', '2020-07-15 15:34:30'),
(57, 2, 1010, 'Ingreso el siguiente proveedor:<br>identificación: 1010<br>correo: toy@toy.com', '2020-07-15 15:53:55'),
(58, 2, 1010, 'Ingreso el siguiente proveedor:<br>identificación: 1010<br>correo: toy@toy.com', '2020-07-15 16:29:27'),
(59, 5, 1010, 'se edito el siguiente vehiculo:<br>identificacion: 1000018<br>Se ingresaron los siguientes datos:<br>identificación de fabrica: 215<br>identificación del proveedor: 1010<br>identificación del nombre: 100017<br>modelo: 2020<br>precio: 250000000', '2020-07-15 16:29:59'),
(60, 5, 1010, 'se edito el siguiente vehiculo:<br>identificacion: 1000018<br>Se ingresaron los siguientes datos:<br>identificación de fabrica: 1111<br>identificación del proveedor: 1010<br>identificación del nombre: 100017<br>modelo: 2020<br>precio: 1', '2020-07-15 16:33:58'),
(61, 5, 1010, 'se edito el siguiente vehiculo:\r\nidentificacion: 1000018\r\nSe ingresaron los siguientes datos:\r\nidentificación de fabrica: 1111\r\nidentificación del proveedor: 1010\r\nidentificación del nombre: 100017\r\nmodelo: 2020\r\nprecio: 1', '2020-07-15 16:40:42'),
(62, 5, 1010, 'se edito el siguiente vehiculo:<br>identificacion: 1000018<br>Se ingresaron los siguientes datos:<br>identificación de fabrica: 45456<br>identificación del proveedor: 1010<br>identificación del nombre: 100017<br>modelo: 2020<br>precio: 600000000', '2020-07-15 16:43:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nombrev`
--

CREATE TABLE `nombrev` (
  `idnombrev` int(35) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `nombrev`
--

INSERT INTO `nombrev` (`idnombrev`, `nombre`) VALUES
(100000, 'Serie 1'),
(100001, 'Serie 2'),
(100002, 'Serie 3'),
(100003, 'Serie 4'),
(100004, 'Z4 Roadster'),
(100005, 'Model S'),
(100007, 'X6'),
(100008, 'Serie 7'),
(100009, 'M2'),
(100010, 'Amg'),
(100011, 'Clase g'),
(100012, 'Aventator'),
(100013, 'Camaro'),
(100014, '458i'),
(100015, 'Rs 6'),
(100016, 'R 8'),
(100017, 'Supra'),
(100018, 'Tacoma'),
(100019, 'Grand Cherokee');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idproveedor` int(35) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `nit` int(30) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idproveedor`, `nombre`, `correo`, `clave`, `estado`, `nit`, `foto`) VALUES
(1001, 'BMW', '200@200.com', '3644a684f98ea8fe223c713b77189a77', 1, 14552589, NULL),
(1002, 'Mercedes', '400@400.com', '18d8042386b79e2c279fd162df0205c8', 1, 12558, NULL),
(1003, 'Chevrolet', '900@900.com', 'acf4b89d3d503d8252c9c4ba75ddbf6d', 1, 12558, 'Imagenes/Proveedor/1593754628.jpg'),
(1004, 'Lamborghini', '901@901.com', '892c91e0a653ba19df81a90f89d99bcd', 1, 12558, 'Imagenes/Proveedor/1593754813.png'),
(1005, 'Renault', '902@902.com', 'b6a1085a27ab7bff7550f8a3bd017df8', 0, 2355, ''),
(1006, 'Tesla', '905@905.com', 'f57a2f557b098c43f11ab969efe1504b', 1, 3322155, 'Imagenes/Proveedor/1593821820.jpg'),
(1007, 'McLaren', '999@999.com', 'b706835de79a2b4e80506f582af3676a', 1, 326522365, 'Imagenes/Proveedor/1594569738.png'),
(1008, 'Audi', 'au@au.com', '8bcc25c96aa5a71f7a76309077753e67', 1, 23652589, 'Imagenes/Proveedor/1594759720.png'),
(1009, 'Ferrari', 'fe@fe.com', '2d917f5d1275e96fd75e6352e26b1387', 1, 6262664, 'Imagenes/Proveedor/1594759767.jpg'),
(1010, 'Toyota', 'toy@toy.com', '10016b6ed5a5b09be08133fa2d282636', 1, 62626556, 'Imagenes/Proveedor/1594760067.jpg'),
(1011, 'Jeep', 'je@je.com', '79563e90630af3525dff01b6638b0886', 1, 455522, 'Imagenes/Proveedor/1594760337.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `idvehiculo` bigint(50) NOT NULL,
  `idfabrica` int(35) NOT NULL,
  `idproveedor_FK` int(35) NOT NULL,
  `idnombrev_FK` int(35) NOT NULL,
  `estado` int(1) DEFAULT NULL,
  `modelo` int(20) DEFAULT NULL,
  `precio` bigint(50) DEFAULT NULL,
  `foto` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`idvehiculo`, `idfabrica`, `idproveedor_FK`, `idnombrev_FK`, `estado`, `modelo`, `precio`, `foto`) VALUES
(1000000, 125488652, 1001, 100000, 0, 2020, 41000000, 'https://e00-marca.uecdn.es/assets/multimedia/imagenes/2019/07/17/15633525212042.jpg'),
(1000001, 21251202, 1001, 100001, 0, 2020, 41000000, 'https://www.ecestaticos.com/imagestatic/clipping/557/3ad/5573ad37a99030db3990683548dd7ef7/el-razonable-precio-del-bmw-serie-2-gran-coupe-y-por-que-es-un-coche-tan-elegante.jpg?mtime=1584723662'),
(1000002, 455115, 1001, 100002, 0, 2020, 41000000, 'https://e00-marca.uecdn.es/assets/multimedia/imagenes/2018/12/12/15446333612813.jpg'),
(1000003, 565655, 1001, 100003, 0, 2020, 41000000, 'https://www.motor.es/fotos-noticias/2019/04/bmw-serie-4-coupe-recreaciones-201956812-1556181791_1.jpg'),
(1000004, 52512, 1001, 100004, 0, 2020, 410000000, 'https://3.bp.blogspot.com/-ZLNc4PkLeIQ/W39MCIVKPxI/AAAAAAAAHWM/4nd458WRKDgNtD3jCaNH_NIr2bEzY_ipQCLcBGAs/s1600/2020-bmw-z4-01-1.jpg'),
(1000005, 541512, 1001, 100000, 0, 2015, 20000000, 'https://www.diariomotor.com/imagenes/picscache/750x/1024_peq_BMW_serie_1_2015_DM_34_750x.jpg'),
(1000006, 4451512, 1001, 100000, 0, 2020, 41000000, 'https://e00-marca.uecdn.es/assets/multimedia/imagenes/2019/07/17/15633525212042.jpg'),
(1000007, 23331526, 1006, 100005, 0, 2020, 600000000, 'Imagenes/Vehiculo/1593823932.jpg'),
(1000008, 25125, 1001, 100007, 0, 2020, 250000000, 'Imagenes/Vehiculo/1594569237.jpg'),
(1000009, 3233256, 1001, 100008, 1, 2020, 170000000, 'Imagenes/Vehiculo/1594760499.jpg'),
(1000010, 225151, 1001, 100009, 1, 2020, 250000000, 'Imagenes/Vehiculo/1594760523.jpg'),
(1000011, 15152, 1002, 100010, 1, 2020, 250000000, 'Imagenes/Vehiculo/1594760610.jpg'),
(1000012, 233656, 1002, 100011, 1, 2020, 750000000, 'Imagenes/Vehiculo/1594760641.jpg'),
(1000013, 6226, 1004, 100012, 1, 2020, 2700000000, 'Imagenes/Vehiculo/1594760713.jpg'),
(1000014, 262666, 1003, 100013, 1, 2020, 1500000000, 'Imagenes/Vehiculo/1594760790.jpg'),
(1000015, 565112, 1009, 100014, 1, 2020, 2700000000, 'Imagenes/Vehiculo/1594760852.jpg'),
(1000016, 626226, 1008, 100015, 1, 2020, 500000000, 'Imagenes/Vehiculo/1594760932.jpg'),
(1000017, 241523, 1008, 100016, 1, 2020, 350000000, 'Imagenes/Vehiculo/1594760960.jpg'),
(1000018, 45456, 1010, 100017, 1, 2020, 600000000, 'Imagenes/Vehiculo/1594831417.jpg'),
(1000019, 65656, 1010, 100018, 1, 2020, 700000000, 'Imagenes/Vehiculo/1594761037.jpg'),
(1000020, 626262, 1011, 100019, 1, 2020, 350000000, 'Imagenes/Vehiculo/1594761112.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idventa` bigint(50) NOT NULL,
  `idcliente_FK` bigint(50) NOT NULL,
  `fecha` timestamp NULL DEFAULT NULL,
  `valorVenta` bigint(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idventa`, `idcliente_FK`, `fecha`, `valorVenta`) VALUES
(100000029, 10000000, '2020-06-29 22:50:03', 41000000),
(100000030, 10000000, '2020-07-11 02:58:07', 20000000),
(100000031, 10000000, '2020-07-11 16:00:37', 1010000000),
(100000032, 10000000, '2020-07-12 02:27:56', 41000000),
(100000033, 10000000, '2020-07-14 14:31:25', 41000000),
(100000034, 10000007, '2020-07-14 20:04:56', 291000000),
(100000035, 10000007, '2020-07-14 20:05:26', 41000000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventavehiculo`
--

CREATE TABLE `ventavehiculo` (
  `idventa_FK` bigint(50) NOT NULL,
  `idvehiculo_FK` bigint(50) NOT NULL,
  `precio` bigint(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `ventavehiculo`
--

INSERT INTO `ventavehiculo` (`idventa_FK`, `idvehiculo_FK`, `precio`) VALUES
(100000029, 1000001, 41000000),
(100000030, 1000005, 20000000),
(100000031, 1000004, 410000000),
(100000031, 1000007, 600000000),
(100000032, 1000003, 41000000),
(100000033, 1000002, 41000000),
(100000034, 1000000, 41000000),
(100000034, 1000008, 250000000),
(100000035, 1000006, 41000000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acciones`
--
ALTER TABLE `acciones`
  ADD PRIMARY KEY (`idaccion`);

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idadministrador`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `idaccion_FK` (`idaccion_FK`),
  ADD KEY `idadministrador_FK` (`idadministrador_FK`);

--
-- Indices de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `idaccion_FK` (`idaccion_FK`),
  ADD KEY `idcliente_FK` (`idcliente_FK`);

--
-- Indices de la tabla `logproveedor`
--
ALTER TABLE `logproveedor`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `idaccion_FK` (`idaccion_FK`),
  ADD KEY `idproveedor_FK` (`idproveedor_FK`);

--
-- Indices de la tabla `nombrev`
--
ALTER TABLE `nombrev`
  ADD PRIMARY KEY (`idnombrev`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idproveedor`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`idvehiculo`),
  ADD KEY `idproveedor_FK` (`idproveedor_FK`),
  ADD KEY `idnombrev_FK` (`idnombrev_FK`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idventa`),
  ADD KEY `idcliente_FK` (`idcliente_FK`);

--
-- Indices de la tabla `ventavehiculo`
--
ALTER TABLE `ventavehiculo`
  ADD PRIMARY KEY (`idventa_FK`,`idvehiculo_FK`),
  ADD KEY `idvehiculo_FK` (`idvehiculo_FK`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acciones`
--
ALTER TABLE `acciones`
  MODIFY `idaccion` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idadministrador` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10000008;

--
-- AUTO_INCREMENT de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  MODIFY `idlog` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  MODIFY `idlog` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `logproveedor`
--
ALTER TABLE `logproveedor`
  MODIFY `idlog` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `nombrev`
--
ALTER TABLE `nombrev`
  MODIFY `idnombrev` int(35) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100020;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idproveedor` int(35) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1012;

--
-- AUTO_INCREMENT de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  MODIFY `idvehiculo` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000021;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idventa` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100000036;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD CONSTRAINT `logadministrador_ibfk_1` FOREIGN KEY (`idaccion_FK`) REFERENCES `acciones` (`idaccion`),
  ADD CONSTRAINT `logadministrador_ibfk_2` FOREIGN KEY (`idadministrador_FK`) REFERENCES `administrador` (`idadministrador`);

--
-- Filtros para la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD CONSTRAINT `logcliente_ibfk_1` FOREIGN KEY (`idaccion_FK`) REFERENCES `acciones` (`idaccion`),
  ADD CONSTRAINT `logcliente_ibfk_2` FOREIGN KEY (`idcliente_FK`) REFERENCES `cliente` (`idcliente`);

--
-- Filtros para la tabla `logproveedor`
--
ALTER TABLE `logproveedor`
  ADD CONSTRAINT `logproveedor_ibfk_1` FOREIGN KEY (`idaccion_FK`) REFERENCES `acciones` (`idaccion`),
  ADD CONSTRAINT `logproveedor_ibfk_2` FOREIGN KEY (`idproveedor_FK`) REFERENCES `proveedor` (`idproveedor`);

--
-- Filtros para la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD CONSTRAINT `vehiculo_ibfk_1` FOREIGN KEY (`idproveedor_FK`) REFERENCES `proveedor` (`idproveedor`),
  ADD CONSTRAINT `vehiculo_ibfk_2` FOREIGN KEY (`idnombrev_FK`) REFERENCES `nombrev` (`idnombrev`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`idcliente_FK`) REFERENCES `cliente` (`idcliente`);

--
-- Filtros para la tabla `ventavehiculo`
--
ALTER TABLE `ventavehiculo`
  ADD CONSTRAINT `ventavehiculo_ibfk_1` FOREIGN KEY (`idventa_FK`) REFERENCES `venta` (`idventa`),
  ADD CONSTRAINT `ventavehiculo_ibfk_2` FOREIGN KEY (`idvehiculo_FK`) REFERENCES `vehiculo` (`idvehiculo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
