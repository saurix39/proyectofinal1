<?php
class AdministradorDAO{
    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;

    public function AdministradorDAO($idAdministrador="",$nombre="",$apellido="",$correo="",$clave="",$foto="",$estado=""){
        $this -> idAdministrador=$idAdministrador;
        $this -> nombre=$nombre;
        $this -> apellido=$apellido;
        $this -> correo=$correo;
        $this -> clave=$clave;
        $this -> foto=$foto;
        $this -> estado=$estado;
    }

    public function autenticar(){
        return "select idadministrador, estado from administrador where correo='". $this -> correo ."' and clave='". md5($this -> clave) ."'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto from administrador where idadministrador='". $this -> idAdministrador ."'";
    }

    public function existeCorreo(){
        return "select correo from administrador where correo='". $this -> correo ."'";
    }

    public function registrarAdministrador(){
        return "insert into administrador (nombre,apellido,correo,clave,foto,estado) values ('". $this -> nombre ."','". $this -> apellido ."','". $this -> correo ."','". md5($this -> clave) ."','". (($this -> foto != "")?$this -> foto:"") ."','1')";
    }

    public function lista($cantidad,$pagina){
        return "select idadministrador,nombre,apellido,correo,estado
        from administrador
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idadministrador) from administrador";
    }

    public function habilitar(){
        return "update administrador set estado=1
                where idadministrador='". $this -> idAdministrador ."'";
    }

    public function inhabilitar(){
        return "update administrador set estado=0
                where idadministrador='". $this -> idAdministrador ."'";
    }

    public function consultarEstado(){
        return "select estado from administrador where idadministrador='". $this -> idAdministrador ."'";
    }

}
?>