<?php
class LogDAO{
    private $idLog;
    private $idAccion;
    private $idActor;
    private $idTipo;
    private $informacion;
    private $fecha;
    private $hora;
    
    public function LogDAO($idLog="",$idAccion="",$idActor="",$idTipo="",$informacion="",$fecha="",$hora=""){
        $this -> idLog=$idLog;
        $this -> idAccion=$idAccion;
        $this -> idActor=$idActor;
        $this -> idTipo=$idTipo;
        $this -> informacion=$informacion;
        $this -> fecha=$fecha;
        $this -> hora=$hora;
    }

    public function insertarLogCliente(){
        return "insert into logcliente (idaccion_FK,idcliente_FK,informacion,fecha_hora) values ('". $this -> idAccion ."','". $this -> idActor ."','". $this -> informacion ."','". $this -> fecha ."')";
    }

    public function insertarLogAdministrador(){
        return "insert into logadministrador (idaccion_FK,idadministrador_FK,informacion,fecha_hora) values ('". $this -> idAccion ."','". $this -> idActor ."','". $this -> informacion ."','". $this -> fecha ."')";
    }

    public function insertarLogProveedor(){
        return "insert into logproveedor (idaccion_FK,idproveedor_FK,informacion,fecha_hora) values ('". $this -> idAccion ."','". $this -> idActor ."','". $this -> informacion ."','". $this -> fecha ."')";
    }

    public function consultarLogRegistroAdmin($filtro){
        return "select idlog,idaccion_FK,idadministrador_FK,informacion,fecha_hora 
        from logadministrador 
        where idaccion_FK='1' and (idadministrador_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogRegistroClien($filtro){
        return "select idlog,idaccion_FK,idcliente_FK,informacion,fecha_hora 
        from logcliente 
        where idaccion_FK='1' and (idcliente_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogRegistroProv($filtro){
        return "select idlog,idaccion_FK,idproveedor_FK,informacion,fecha_hora 
        from logproveedor 
        where idaccion_FK='1' and (idproveedor_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogIngresoAdmin($filtro){
        return "select idlog,idaccion_FK,idadministrador_FK,informacion,fecha_hora
        from logadministrador 
        where idaccion_FK='2' and (idadministrador_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogIngresoClien($filtro){
        return "select idlog,idaccion_FK,idcliente_FK,informacion,fecha_hora 
        from logcliente  
        where idaccion_FK='2' and (idcliente_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogIngresoProv($filtro){
        return "select idlog,idaccion_FK,idproveedor_FK,informacion,fecha_hora 
        from logproveedor 
        where idaccion_FK='2' and (idproveedor_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogVentaClien($filtro){
        return "select idlog,idaccion_FK,idcliente_FK,informacion,fecha_hora 
        from logcliente 
        where idaccion_FK='3' and (idcliente_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogEstadoAdmin($filtro){
        return "select idlog,idaccion_FK,idadministrador_FK,informacion,fecha_hora 
        from logadministrador 
        where idaccion_FK='4' and (idadministrador_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogEstadoProv($filtro){
        return "select idlog,idaccion_FK,idproveedor_FK,informacion,fecha_hora 
        from logproveedor
        where idaccion_FK='4' and (idproveedor_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarInfo(){
        return "select idaccion_FK,informacion,fecha_hora from ". (($this -> idTipo==1)?"logadministrador":(($this -> idTipo==2)?"logcliente":"logproveedor")) ." where idlog='". $this -> idLog ."'";
    }

    public function nombreAccion(){
        return "select nombreaccion from acciones where idaccion='". $this -> idAccion ."'";
    }

    public function consultarLogEdicionProv($filtro){
        return "select idlog,idaccion_FK,idproveedor_FK,informacion,fecha_hora 
        from logproveedor 
        where idaccion_FK='5' and (idproveedor_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function consultarLogEdicionClien($filtro){
        return "select idlog,idaccion_FK,idcliente_FK,informacion,fecha_hora 
        from logcliente 
        where idaccion_FK='5' and (idcliente_FK like '%". $filtro ."%' or fecha_hora like '%". $filtro ."%')";
    }

    public function setIdAccion($idAccion)
    {
        $this->idAccion = $idAccion;

        return $this;
    }
}
?>