<?php
class VentaDAO{
    private $idVenta;
    private $idCliente;
    private $fecha;
    private $valorVenta;
    
    public function VentaDAO($idVenta="",$idCliente="",$fecha="",$valorVenta=""){
        $this -> idVenta=$idVenta;
        $this -> idCliente=$idCliente;
        $this -> fecha=$fecha;
        $this -> valorVenta=$valorVenta;
    }

    public function insertarVenta(){
        return "insert into venta (idcliente_FK,fecha,valorVenta) values ('". $this -> idCliente ."','". $this -> fecha ."', '". $this -> valorVenta ."')";
    }

    public function consultar(){
        return "select idventa from venta where idcliente_FK='". $this -> idCliente ."' and fecha='". $this -> fecha ."' and valorVenta='". $this -> valorVenta ."'";
    }

    public function ingresarVehiculos($idvehiculo,$preciovehiculo){
        return "insert into ventavehiculo (idventa_FK,idvehiculo_FK,precio) values ('". $this -> idVenta ."','". $idvehiculo ."','". $preciovehiculo ."')";
    }

    public function consultarVentasCliente($cantidad, $pagina){
        return "select idventa, idcliente_FK, fecha, valorVenta
        from venta
        where idcliente_FK='". $this -> idCliente ."'
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadCliente(){
        return "select count(idventa) from venta where idcliente_FK='". $this -> idCliente ."'";
    }

    public function consultarVentas($cantidad, $pagina){
        return "select idventa, idcliente_FK, fecha, valorVenta
        from venta
        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idventa) from venta";
    }

    public function consultarInformacion(){
        return "select idcliente_FK, fecha, valorVenta from venta where idventa='". $this -> idVenta ."'";
    }

    public function consultarVehiculosVenta(){
        return "select idvehiculo_FK, vv.precio, v.idnombrev_fk,v.idproveedor_fk
                from ventavehiculo vv inner join vehiculo v
                on (idvehiculo_fk=idvehiculo)
                where idventa_fk='". $this -> idVenta ."'";
    }

    public function setIdVenta($idVenta)
    {
        $this->idVenta = $idVenta;

        return $this;
    }
}
?>