<?php
class VehiculoDAO{
    private $idVehiculo; 
    private $idFabrica;
    private $idProveedor;
    private $idNombre;
    private $estado;
    private $modelo;
    private $precio;
    private $foto;

    public function VehiculoDAO($idVehiculo="",$idFabrica="",$idProveedor="",$idNombre="",$estado="",$modelo="",$precio="",$foto=""){
        $this -> idVehiculo = $idVehiculo;
        $this -> idFabrica = $idFabrica;
        $this -> idProveedor = $idProveedor;
        $this -> idNombre = $idNombre;
        $this -> estado = $estado;
        $this -> modelo = $modelo;
        $this -> precio = $precio;
        $this -> foto = $foto;
    }

    public function consultar(){
        return "select precio, foto
                from vehiculo 
                where idnombrev_fk='". $this -> idNombre ."' and idproveedor_fk='". $this -> idProveedor ."' and modelo='". $this -> modelo ."'
                limit 1";
    }

    public function consultarPaginacion($cantidad, $pagina){
        return "select distinct idnombrev_fk, idproveedor_fk, modelo, foto
                from vehiculo 
                where estado=1
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function nombreProveedor(){
        return "select nombre
                from proveedor
                where idproveedor='". $this -> idProveedor ."'";
    }

    public function nombreVehiculo(){
        return "select nombre
                from nombrev
                where idnombrev='". $this -> idNombre ."'";
    }

    public function consultarCantidad(){
        return "select count(idvehiculo) from vehiculo where estado=1";
    }

    public function consultaringresar(){
        return "select idvehiculo
                from vehiculo 
                where idnombrev_fk='". $this -> idNombre ."' and idproveedor_fk='". $this -> idProveedor ."' and modelo='". $this -> modelo ."' and estado=1
                limit 1";
    }

    public function venderVehiculo(){
        return "update vehiculo set estado=0 where idvehiculo='". $this -> idVehiculo ."'";
    }

    public function lista($cantidad,$pagina){
        return "select idvehiculo,idfabrica,idproveedor_fk,idnombrev_fk,estado,modelo,precio
                from vehiculo
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadT(){
        return "select count(idvehiculo) from vehiculo";
    }

    public function listaProv($cantidad,$pagina){
        return "select idvehiculo,idfabrica,idproveedor_fk,idnombrev_fk,estado,modelo,precio
                from vehiculo where idproveedor_fk='". $this -> idProveedor ."'
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidadProv(){
        return "select count(idvehiculo) from vehiculo where idproveedor_fk='". $this -> idProveedor ."'";
    }

    public function habilitar(){
        return "update vehiculo set estado=1
                where idvehiculo='". $this -> idVehiculo ."'";
    }

    public function inhabilitar(){
        return "update vehiculo set estado=0
                where idvehiculo='". $this -> idVehiculo ."'";
    }

    public function registrarVehiculo(){
        return "insert into vehiculo (idfabrica,idproveedor_fk,idnombrev_fk,estado,modelo,precio,foto) values('". $this -> idFabrica ."','". $this -> idProveedor ."','". $this -> idNombre ."','1','". $this -> modelo ."','". $this -> precio ."','". $this -> foto ."')";
    }

    public function consultarEstado(){
        return "select estado from vehiculo where idvehiculo='". $this -> idVehiculo ."'";
    }

    public function consultarFiltro($filtro){
        return "select distinct idnombrev_fk, idproveedor_fk, modelo, v.foto
                from vehiculo v inner join nombrev n 
                on (idnombrev_fk=idnombrev)
                inner join proveedor p
                on (idproveedor=idproveedor_FK)
                where v.estado=1 and (n.nombre like '%". $filtro ."%' or p.nombre like '%". $filtro ."%' or modelo like '%". $filtro ."%')";
    }

    public function consultarCantidadFiltro($filtro){
        return "select count(idvehiculo) 
        from vehiculo v
        inner join nombrev n 
        on (idnombrev_fk=idnombrev)
        inner join proveedor p
        on (idproveedor=idproveedor_FK)
        where v.estado=1 and (n.nombre like '%". $filtro ."%' or p.nombre like '%". $filtro ."%' or modelo like '%". $filtro ."%')";
    }

    public function consultarEd(){
        return "select precio, foto
                from vehiculo 
                where idvehiculo='". $this -> idVehiculo ."'";
    }

    public function editarvehiculo(){
        return "update vehiculo set idfabrica='". $this -> idFabrica ."', idproveedor_FK='". $this -> idProveedor ."', idnombrev_FK='". $this -> idNombre ."', modelo='". $this -> modelo ."', precio='". $this -> precio ."', foto='". $this -> foto ."'
                where idvehiculo='". $this -> idVehiculo ."'";
    }

    public function setIdProveedor($idProveedor)
    {
        $this->idProveedor = $idProveedor;

        return $this;
    }

    public function setIdNombre($idNombre)
    {
        $this->idNombre = $idNombre;

        return $this;
    }

        public function setIdVehiculo($idVehiculo)
        {
                $this->idVehiculo = $idVehiculo;

                return $this;
        }
}
?>