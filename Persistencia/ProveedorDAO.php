<?php
class ProveedorDAO{
    private $idProveedor;
    private $nombre;
    private $correo;
    private $clave;
    private $estado;
    private $nit;
    private $foto;

    public function ProveedorDAO($idProveedor="",$nombre="",$correo="",$clave="",$estado="",$nit="",$foto=""){
        $this -> idProveedor = $idProveedor;
        $this -> nombre = $nombre;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado = $estado;
        $this -> nit = $nit;
        $this -> foto= $foto;
    }

    public function autenticar(){
        return "select idproveedor, estado from proveedor where correo='". $this -> correo ."' and clave='". md5($this -> clave) ."'";
    }

    public function consultar(){
        return "select nombre, correo, nit, foto from proveedor where idproveedor='". $this -> idProveedor ."'";
    }
    
    public function existeCorreo(){
        return "select correo from proveedor where correo='". $this -> correo ."'";
    }

    public function registrarProveedor(){
        return "insert into proveedor (nombre, correo, clave, estado, nit, foto) values ('". $this -> nombre ."','". $this -> correo ."', '". md5($this -> clave) ."', 1, '". $this -> nit ."','". (($this -> foto != "")?$this -> foto:"") ."')"; 
    }

    public function lista($cantidad,$pagina){
        return "select idproveedor,nombre,correo,estado,nit
                from proveedor
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idproveedor) from proveedor";
    }

    public function habilitar(){
        return "update proveedor set estado=1
                where idproveedor='". $this -> idProveedor ."'";
    }

    public function inhabilitar(){
        return "update proveedor set estado=0
                where idproveedor='". $this -> idProveedor ."'";
    }

    public function registrarNombre($nombre){
        return "insert into nombrev (nombre) values('". $nombre ."')";
    }

    public function existeNombre($nombre){
        return "select idnombrev from nombrev where nombre='". $nombre ."'";
    }

    public function obtenerNombres(){
        return "select nombre from nombrev";
    }

    public function consultarIdNombre($nombre){
        return "select idnombrev from nombrev where nombre='". $nombre ."'";
    }
    
    public function consultarEstado(){
        return "select estado from proveedor where idproveedor='". $this -> idProveedor ."'";
    }
}
?>