<?php
require_once "Logica/Administrador.php";
require_once "Logica/Cliente.php";
require_once "Logica/Proveedor.php";
require_once "Logica/Vehiculo.php";
require_once "Logica/Venta.php";
require_once "Logica/Log.php";


session_start();
$pos="";
if(isset($_GET["pos"])){
    $pos=$_GET["pos"];
    }
$pid="";
if(isset($_GET["pid"])){
$pid=base64_decode($_GET["pid"]);
}else{
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
}
if(isset($_GET["cerrarSesion"])||!isset($_SESSION["id"])){
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
    $_SESSION["Productos"]=array();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="img/iconoP.png"></link>
    <link href="Presentacion/Estilos5.css" rel="stylesheet" type="text/css"></link>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <title>BoostingCo</title>
    <script>
        $(function () {
	    $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

</head>
<body>    
    <?php 
        $paginasSinSesion= array(
            "Presentacion/formAutenticar.php",
            "Presentacion/Autenticar.php",
            "Presentacion/Cliente/registrarCliente.php"
        );
        $paginasDeInicio= array(
            "Presentacion/Servicios.php",
            "Presentacion/sobreNosotros.php"
        );
        if(in_array($pid, $paginasSinSesion)){
            include $pid;
        }else if($_SESSION["id"]!=""){
            if($_SESSION["rol"]=="Administrador"){
                include "Presentacion/Administrador/menuAdministrador.php";
            }else if($_SESSION["rol"]=="Cliente"){
                include "Presentacion/Cliente/menuCliente.php";
            }else if($_SESSION["rol"]=="Proveedor"){
                include "Presentacion/Proveedor/menuProveedor.php";
            }
            include $pid;
            include "Presentacion/footer.php";
        }else{
            include "Presentacion/menu.php";
            if(in_array($pid,$paginasDeInicio)){
                include $pid;
            }else{
                include "Presentacion/inicio.php";  
            }
            include "Presentacion/footer.php";
        }
        
    ?>
</body>
</html>