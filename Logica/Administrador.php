<?php
require_once "Persistencia/AdministradorDAO.php";
require_once "Persistencia/Conexion.php";

class Administrador{
    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $administradorDAO;

    public function Administrador($idAdministrador="",$nombre="",$apellido="",$correo="",$clave="",$foto="",$estado=""){
        $this -> idAdministrador=$idAdministrador;
        $this -> nombre=$nombre;
        $this -> apellido=$apellido;
        $this -> correo=$correo;
        $this -> clave=$clave;
        $this -> foto=$foto;
        $this -> estado=$estado;
        $this -> conexion= new Conexion();
        $this -> administradorDAO=new AdministradorDAO($idAdministrador,$nombre,$apellido,$correo,$clave,$foto,$estado);
    }

    public function autenticar(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> administradorDAO -> autenticar());
        $this -> conexion ->cerrar();
        if($this -> conexion -> numFilas()==1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAdministrador = $resultado[0];
            $this -> estado = $resultado[1];
            return 1;
        }else{
            return 0;
        }

    }

    public function consultar(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> administradorDAO -> consultar());
        $this -> conexion ->cerrar();
        $resultado= $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto=$resultado[3];
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()>=1){
            return 1;
        }else{
            return 0;
        }
    }

    public function registrarAdministrador(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> registrarAdministrador());
        $this -> conexion -> cerrar();
    }

    public function lista($cantidad,$pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> lista($cantidad,$pagina));
        $administradores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $a = new Administrador($resultado[0],$resultado[1],$resultado[2],$resultado[3],"","",$resultado[4]);
            array_push($administradores, $a);
        }
        $this -> conexion -> cerrar();
        return $administradores;
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function habilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> habilitar());
        $this -> conexion -> cerrar();
    }

    public function inhabilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> inhabilitar());
        $this -> conexion -> cerrar();
    }

    public function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarEstado());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function getIdAdministrador()
    {
        return $this->idAdministrador;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }
 
    public function getClave()
    {
        return $this->clave;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function getAdministradorDAO()
    {
        return $this->administradorDAO;
    }

    public function getFoto()
    {
        return $this->foto;
    }

    public function getEstado()
    {
        return $this->estado;
    }
}
?>