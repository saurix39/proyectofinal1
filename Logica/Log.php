<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/LogDAO.php";
class Log{
    private $idLog;
    private $idAccion;
    private $idActor;
    private $idTipo;
    private $informacion;
    private $fecha;
    private $hora;
    private $conexion;
    private $logDAO;
    
    public function Log($idLog="",$idAccion="",$idActor="",$idTipo="",$informacion="",$fecha="",$hora=""){
        $this -> idLog=$idLog;
        $this -> idAccion=$idAccion;
        $this -> idActor=$idActor;
        $this -> idTipo=$idTipo;
        $this -> informacion=$informacion;
        $this -> fecha=$fecha;
        $this -> hora=$hora;
        $this -> conexion=new Conexion();
        $this -> logDAO=new LogDAO($idLog,$idAccion,$idActor,$idTipo,$informacion,$fecha,$hora);

    }

    public function insertarLogCliente(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarLogCliente());
        $this -> conexion -> cerrar();
    }

    public function insertarLogAdministrador(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarLogAdministrador());
        $this -> conexion -> cerrar();
    }

    public function insertarLogProveedor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> insertarLogProveedor());
        $this -> conexion -> cerrar();
    }

    public function consultarLogRegistro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogRegistroAdmin($filtro));
        $Logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"1",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogRegistroClien($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"2",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogRegistroProv($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"3",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> cerrar();
        return $Logs;
    }

    public function consultarLogIngreso($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogIngresoAdmin($filtro));
        $Logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"1",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogIngresoClien($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"2",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogIngresoProv($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"3",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> cerrar();
        return $Logs;
    }

    public function consultarLogVenta($filtro){
        $this -> conexion -> abrir();
        $Logs = array();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogVentaClien($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"2",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> cerrar();
        return $Logs;
    }

    public function consultarLogEstado($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogEstadoAdmin($filtro));
        $Logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"1",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogEstadoProv($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"3",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> cerrar();
        return $Logs;
    }

    public function consultarInfo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarInfo());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $fecha_hora = preg_split("/ /",$resultado[2]);
        $this -> idAccion = $resultado[0];
        $this -> logDAO -> setIdAccion($resultado[0]);
        $this -> informacion = $resultado[1];
        $this -> fecha = $fecha_hora[0];
        $this -> hora = $fecha_hora[1];
    }

    public function nombreAccion(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> nombreAccion());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function consultarLogEdicion($filtro){
        $this -> conexion -> abrir();
        $Logs = array();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogEdicionClien($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"2",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> ejecutar($this -> logDAO -> consultarLogEdicionProv($filtro));
        while(($resultado = $this -> conexion -> extraer()) != null){
            $fechahora=preg_split("/ /",$resultado[4]);
            $l = new Log($resultado[0],$resultado[1],$resultado[2],"3",$resultado[3],$fechahora[0],$fechahora[1]);
            array_push($Logs, $l);
        }
        $this -> conexion -> cerrar();
        return $Logs;
    }

 
    public function getIdLog()
    {
        return $this->idLog;
    }

    public function getIdAccion()
    {
        return $this->idAccion;
    }

    public function getIdActor()
    {
        return $this->idActor;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function getInformacion()
    {
        return $this->informacion;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }
}
?>