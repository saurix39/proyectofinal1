<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/VehiculoDAO.php";

class Vehiculo{
    private $idVehiculo; 
    private $idFabrica;
    private $idProveedor;
    private $idNombre;
    private $estado;
    private $modelo;
    private $precio;
    private $foto;
    private $conexion;
    private $vehiculoDAO;

    public function Vehiculo($idVehiculo="",$idFabrica="",$idProveedor="",$idNombre="",$estado="",$modelo="",$precio="",$foto=""){
        $this -> idVehiculo = $idVehiculo;
        $this -> idFabrica = $idFabrica;
        $this -> idProveedor = $idProveedor;
        $this -> idNombre = $idNombre;
        $this -> estado = $estado;
        $this -> modelo = $modelo;
        $this -> precio = $precio;
        $this -> foto = $foto;
        $this -> conexion = new Conexion();
        $this -> vehiculoDAO = new VehiculoDAO($idVehiculo,$idFabrica,$idProveedor,$idNombre,$estado,$modelo,$precio,$foto);
    }

    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarPaginacion($cantidad, $pagina));
        $vehiculos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Vehiculo("","",$resultado[1],$resultado[0],"",$resultado[2],"",$resultado[3]);
            array_push($vehiculos, $v);
        }
        $this -> conexion -> cerrar();
        return $vehiculos;
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> precio = $resultado[0];
        $this -> foto = $resultado[1];
    }

    public function consultarEd(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarEd());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> precio = $resultado[0];
        $this -> foto = $resultado[1];
    }

    public function nombreProveedor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> nombreProveedor());
        $this -> conexion -> cerrar();
        $resultado=$this -> conexion ->extraer();
        return $resultado[0];
    }

    public function nombreVehiculo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> nombreVehiculo());
        $this -> conexion -> cerrar();
        $resultado=$this -> conexion ->extraer();
        return $resultado[0];
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this-> conexion -> extraer()[0];
    }

    public function consultaringresar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultaringresar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idVehiculo = $resultado[0];
        $this -> vehiculoDAO -> setIdVehiculo($resultado[0]); 
    }

    public function venderVehiculo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> venderVehiculo());
        $this -> conexion -> cerrar();
    }

    public function lista($cantidad,$pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> lista($cantidad,$pagina));
        $vehiculos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Vehiculo($resultado[0],$resultado[1],$resultado[2],$resultado[3],$resultado[4],$resultado[5],$resultado[6]);
            array_push($vehiculos, $v);
        }
        $this -> conexion -> cerrar();
        return $vehiculos;
    }

    public function consultarCantidadT(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarCantidadT());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function listaProv($cantidad,$pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> listaProv($cantidad,$pagina));
        $vehiculos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Vehiculo($resultado[0],$resultado[1],$resultado[2],$resultado[3],$resultado[4],$resultado[5],$resultado[6]);
            array_push($vehiculos, $v);
        }
        $this -> conexion -> cerrar();
        return $vehiculos;
    }

    public function consultarCantidadProv(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarCantidadProv());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function habilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> habilitar());
        $this -> conexion -> cerrar();
    }

    public function inhabilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> inhabilitar());
        $this -> conexion -> cerrar();
    }

    public function registrarVehiculo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> registrarVehiculo());
        $this -> conexion -> cerrar();
    }

    public function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarEstado());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarFiltro($filtro));
        $vehiculos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Vehiculo("","",$resultado[1],$resultado[0],"",$resultado[2],"",$resultado[3]);
            array_push($vehiculos, $v);
        }
        $this -> conexion -> cerrar();
        return $vehiculos;
    }

    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();
        return $this-> conexion -> extraer()[0];
    }

    public function editarVehiculo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> vehiculoDAO -> editarVehiculo());
        $this -> conexion -> cerrar();
    }

    public function getIdVehiculo()
    {
        return $this->idVehiculo;
    }

    public function getIdFabrica()
    {
        return $this->idFabrica;
    }

    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    public function getIdNombre()
    {
        return $this->idNombre;
    }
 
    public function getEstado()
    {
        return $this->estado;
    }

    public function getModelo()
    {
        return $this->modelo;
    }
 
    public function getPrecio()
    {
        return $this->precio;
    }

    public function getFoto()
    {
        return $this->foto;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function getVehiculoDAO()
    {
        return $this->vehiculoDAO;
    }
}
?>