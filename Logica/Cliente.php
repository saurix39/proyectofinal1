<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ClienteDAO.php";

class Cliente{
    private $idCliente;
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $direccion;
    private $foto;
    private $estado;
    private $conexion;
    private $clienteDAO;

    public function Cliente($idCliente="",$id="",$nombre="",$apellido="",$correo="",$clave="",$direccion="",$foto="",$estado=""){
        $this -> idCliente = $idCliente;
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> direccion = $direccion;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> clienteDAO = new ClienteDAO($idCliente,$id,$nombre,$apellido,$correo,$clave,$direccion,$foto,$estado);

    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()==1){
            $resultado=$this -> conexion -> extraer();
            $this -> idCliente = $resultado[0];
            $this -> estado = $resultado[1];
            return 1;
        }else{
            return 0;
        }
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer(); 
        $this -> id = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> apellido = $resultado[2];
        $this -> correo = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> direccion = $resultado[5];

    }
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()>=1){
            return 1;
        }else{
            return 0;
        }
    }

    public function registrarCliente(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> registrarCliente());
        $this -> conexion -> cerrar();
    }

    public function lista($cantidad,$pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> lista($cantidad,$pagina));
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cliente($resultado[0],$resultado[1],$resultado[2],$resultado[3],$resultado[4],"",$resultado[5],"",$resultado[6]);
            array_push($clientes, $c);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function habilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> habilitar());
        $this -> conexion -> cerrar();
    }

    public function inhabilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> inhabilitar());
        $this -> conexion -> cerrar();
    }

    public function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarEstado());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function actualizarInformacion(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> actualizarInformacion());
        $this -> conexion -> cerrar();
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function getClienteDAO()
    {
        return $this->clienteDAO;
    }
 
    public function getFoto()
    {
        return $this->foto;
    }
}
?>