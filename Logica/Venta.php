<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/VentaDAO.php";

class Venta{
    private $idVenta;
    private $idCliente;
    private $fecha;
    private $valorVenta;
    private $conexion;
    private $ventaDAO;
    
    public function Venta($idVenta="",$idCliente="",$fecha="",$valorVenta=""){
        $this -> idVenta=$idVenta;
        $this -> idCliente=$idCliente;
        $this -> fecha=$fecha;
        $this -> valorVenta=$valorVenta;
        $this -> conexion=new Conexion();
        $this -> ventaDAO=new ventaDAO($idVenta,$idCliente,$fecha,$valorVenta);

    }

    public function insertarVenta(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> ventaDAO -> insertarVenta());
        $this -> conexion ->cerrar();
    }

    public function consultar(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> ventaDAO -> consultar());
        $this -> conexion ->cerrar();
        $resultado=$this -> conexion -> extraer();
        $this -> idVenta = $resultado[0];
        $this -> ventaDAO -> setIdVenta($resultado[0]);

    }

    public function ingresarVehiculos($vehiculos){
        $this -> conexion ->abrir();
        foreach($vehiculos as $vehiculoActual){
            $vehiculoActual -> consultaringresar();
            $this -> conexion ->ejecutar($this -> ventaDAO -> ingresarVehiculos($vehiculoActual -> getIdVehiculo(), $vehiculoActual -> getPrecio()));
            $vehiculoActual -> venderVehiculo();
        }
        $this -> conexion ->cerrar();
    }

    public function consultarVentasCliente($cantidad, $pagina){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> ventaDAO -> consultarVentasCliente($cantidad, $pagina));
        $ventas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Venta($resultado[0],$resultado[1],$resultado[2],$resultado[3]);
            array_push($ventas, $v);
        }
        $this -> conexion -> cerrar();
        return $ventas;
    }

    public function consultarCantidadCliente(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ventaDAO -> consultarCantidadCliente());
        $this -> conexion -> cerrar();
        return $this-> conexion -> extraer()[0];
    }

    public function consultarVentas($cantidad, $pagina){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> ventaDAO -> consultarVentas($cantidad, $pagina));
        $ventas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Venta($resultado[0],$resultado[1],$resultado[2],$resultado[3]);
            array_push($ventas, $v);
        }
        $this -> conexion -> cerrar();
        return $ventas;
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ventaDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this-> conexion -> extraer()[0];
    }

    public function consultarInformacion(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> ventaDAO -> consultarInformacion());
        $this -> conexion ->cerrar();
        $resultado=$this -> conexion -> extraer();
        $this -> idCliente = $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> valorVenta = $resultado[2];
    }

    public function consultarVehiculosVenta(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ventaDAO -> consultarVehiculosVenta());
        $vehiculos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $v = new Vehiculo($resultado[0],"",$resultado[3],$resultado[2],"","",$resultado[1]);
            array_push($vehiculos, $v);
        }
        $this -> conexion -> cerrar();
        return $vehiculos;
    }

    public function getIdVenta()
    {
        return $this->idVenta;
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getValorVenta()
    {
        return $this->valorVenta;
    }
 
    public function getConexion()
    {
        return $this->conexion;
    }

    public function getVentaDAO()
    {
        return $this->ventaDAO;
    }
 
}
?>