<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ProveedorDAO.php";

class Proveedor{
    private $idProveedor;
    private $nombre;
    private $correo;
    private $clave;
    private $estado;
    private $nit;
    private $foto;
    private $conexion;
    private $proveedorDAO;

    public function Proveedor($idProveedor="",$nombre="",$correo="",$clave="",$estado="",$nit="",$foto=""){
        $this -> idProveedor = $idProveedor;
        $this -> nombre = $nombre;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado = $estado;
        $this -> nit = $nit;
        $this -> foto= $foto;
        $this -> conexion = new Conexion();
        $this -> proveedorDAO = new ProveedorDAO($idProveedor,$nombre,$correo,$clave,$estado,$nit,$foto);
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()==1){
            $resultado=$this -> conexion -> extraer();
            $this -> idProveedor = $resultado[0];
            $this -> estado = $resultado[1];
            return 1;
        }else{
            return 0;
        }
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> correo = $resultado[1];
        $this -> nit = $resultado[2];
        $this -> foto = $resultado[3];
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()>=1){
            return 1;
        }else{
            return 0;
        }
    }

    public function registrarProveedor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> registrarProveedor());
        $this -> conexion -> cerrar();
    }

    public function lista($cantidad,$pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> lista($cantidad,$pagina));
        $proveedores = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Proveedor($resultado[0],$resultado[1],$resultado[2],"",$resultado[3],$resultado[4]);
            array_push($proveedores, $p);
        }
        $this -> conexion -> cerrar();
        return $proveedores;
    }

    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function habilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> habilitar());
        $this -> conexion -> cerrar();
    }

    public function inhabilitar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> inhabilitar());
        $this -> conexion -> cerrar();
    }

    public function registrarNombre($nombre){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> registrarNombre($nombre));
        $this -> conexion -> cerrar();
    }

    public function existeNombre($nombre){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> existeNombre($nombre));
        $this -> conexion -> cerrar();
        if($this -> conexion ->numFilas()>=1){
            return 1;
        }else{
            return 0;
        }
    }

    public function obtenerNombres(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> obtenerNombres());
        $nombres=array();
        while(($resultado = $this -> conexion -> extraer()) !=null){
            array_push($nombres,$resultado[0]);
        }
        $this -> conexion -> cerrar();
        return $nombres;
    }

    public function consultarIdNombre($nombre){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarIdNombre($nombre));
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function consultarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarEstado());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getClave()
    {
        return $this->clave;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function getNit()
    {
        return $this->nit;
    }

    public function getConexion()
    {
        return $this->conexion;
    }

    public function getProveedorDAO()
    {
        return $this->proveedorDAO;
    }

    public function getFoto()
    {
        return $this->foto;
    }
}
?>