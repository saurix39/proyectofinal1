<div class="container-fluid">
    <div class="row mt-3">
        <div class="col">
            <div class="card-body infor">
                <div class="row">
                    <div class="col-md-3 col-0">
                    </div>
                    <div class="col-md-3 col-8 text-center dese">
                        <br>
                        <br>
                        <br>
                        <h1>Visión</h1>
                        <p>BoostingCo pretende ser el distribuidor de vehiculos mas
                            grande y reconocido en Colombia, para el 2025 se plane abarcar el 95% del mercado de comercialización de vehiculos en el país.
                        </p>
                    </div>
                    <div class="col-md-3 col-8 text-center dese">
                        <br>
                        <br>
                        <br>
                        <h1>Misión</h1>
                        <p> 
                            Nuestra mision es importar y comercializar vehiculos de todas las gamas
                            a un precio razonable; ademas, no hacemos cargo de los procesos de legalizacion de cada vehiculo como tambien 
                            nos preocupamos por entregar un producto completamente legal y vigente para salir a rodar a las calles colombianas.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>