<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body text-white bg-dark mt-3 text-center">
                <div class="row">
                    <div class="col">
                        <img src="Img/iconoP.png" class="rounded" width="6%"alt="">
                        <br>
                        <hr color="white">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h2 class="card-title">BoostingCo empresa de automoviles</h2>
                        <p class="card-text">©BoostingCo 2020
                        <br>
                        Todos los derechos reservados
                        <br>
                        NIT 99958.22589.322
                        <br>
                        Para reportar errores comuniquese a: BoostingCoErrores@gmail.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>