<?php
$actualizado=false;
$error=0;
if(isset($_POST["registrar"])){
    $cedula=$_POST["cedula"];
    $nombre=$_POST["nombre"];
    $apellido=$_POST["apellido"];
    $direccion=$_POST["direccion"];
    if($_FILES["imagen"]["name"]!=""){
        $rutalocal = $_FILES["imagen"]["tmp_name"];
        $tipo= $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaremota = "Imagenes/Cliente/". $tiempo -> getTimestamp() . (($tipo=="image/png")?".png":".jpg");
        copy($rutalocal,$rutaremota);
        $cliente = new Cliente($_SESSION["id"]);
        $cliente -> consultar();
        if($cliente -> getFoto()!=""){
            unlink($cliente -> getFoto());
        }
        $cliente = new Cliente($_SESSION["id"],$_POST["cedula"],$_POST["nombre"],$_POST["apellido"],"","",$_POST["direccion"],$rutaremota);
        $cliente -> actualizarInformacion();
        $informacion="se edito el siguiente cliente:<br>identificacion: ". $_SESSION["id"] ."<br>Se ingresaron los siguientes datos:<br>cedula: ". $_POST["cedula"] ."<br>nombre: ".$_POST["nombre"]."<br>apellido: ". $_POST["apellido"] . "<br>direccion: ".$_POST["direccion"];
        date_default_timezone_set("America/Bogota");
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","5",$_SESSION["id"],"",$informacion,$fecha);
        $log -> insertarLogCliente();
        $actualizado=true;
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body bg-dark mt-2 text-white">
                <div class="row">
                    <div class="col-md-2 col-0">
                    </div>    
                    <div class="col-md-8 col-12">   
                        <div class="card-header border border-white">
                            <center><h1>Actualizar información</h1></center>
                        </div>
                        <div class="card-body border border-white mt-1">
                            <form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/actualizarInformacion.php")?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="text-white">Cedula de ciudadania</label>  
                                    <input type="text" name="cedula" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Nombre</label>  
                                    <input type="text" name="nombre" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Apellido</label>  
                                    <input type="text" name="apellido" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Dirección</label>  
                                    <input type="text" name="direccion" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Imagen</label> 
                                    <input type="file" name="imagen" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="registrar" class="form-control btn btn-secondary">Actualizar información</button>
                                </div>
                                <?php if($actualizado){ ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Datos ingresados con exito!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php } ?>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>