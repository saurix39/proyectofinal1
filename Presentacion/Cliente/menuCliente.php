<?php
$cliente= new Cliente($_SESSION["id"]);
$cliente -> consultar();
$items=0;
if($cliente -> getId()!=null){
  $items++;
}
if($cliente -> getNombre()!=null){
$items++;
}
if($cliente -> getApellido()!=null){
    $items++;
}
if($cliente -> getCorreo()!=null){
    $items++;
}
if($cliente -> getFoto()!=null){
    $items++;
}
if($cliente -> getDireccion()!=null){
  $items++;
}
$porcentaje=round(($items*100)/6);
?>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/sesionCliente.php")?>">
          <img src="Img/iconoP.png" width="30" height="30" class="d-inline-block align-top rounded" alt="" loading="lazy">
          <strong>BoostingCo</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Vehiculos
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/catalogo.php") ?>">Catalogo</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/facturas.php")?>">Facturas</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/carrito.php") ?>"><i class="fas fa-shopping-cart"></i> Carrito de compras</a>
            </li>
          </ul>
          <ul class="navbar-nav">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="badge badge-<?php echo ($porcentaje==100)?"success":"danger" ?>"><?php echo $porcentaje ?>%</span>
                Cliente: <?php echo ($cliente -> getNombre() != "" && $cliente -> getApellido() != "")?$cliente -> getNombre()." ".$cliente -> getApellido(): $cliente -> getCorreo() ?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/actualizarInformacion.php") ?>">Editar perfil</a>
                <a class="dropdown-item" href="#">Editar foto</a>
                <a class="dropdown-item" href="#">Cambiar clave</a>
              </div>
            </li>
            <form class="form-inline" action="index.php?cerrarSesion=1" method="post">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Cerrar sesión</button>
            </form> 
          </ul>
        </div>
      </nav>
    </div>
  </div>
</div>