<?php
$cliente= new Cliente($_SESSION["id"]);
$cliente -> consultar();
$items=0;
$error=0;
if($cliente -> getId()!=null){
    $items++;
}
if($cliente -> getNombre()!=null){
$items++;
}
if($cliente -> getApellido()!=null){
    $items++;
}
if($cliente -> getCorreo()!=null){
    $items++;
}
if($cliente -> getFoto()!=null){
    $items++;
}
if($cliente -> getDireccion()!=null){
  $items++;
}
$porcentaje=round(($items*100)/6);
$eliminado=false;
if(isset($_GET["eliminado"])){
$eliminado=$_GET["eliminado"];
}
$confirmado=false;
if(isset($_GET["confirmado"])){
    $confirmado=$_GET["confirmado"];
}
if($eliminado){
    foreach($_SESSION["Productos"] as $vehiculoBorra){
        if($vehiculoBorra -> getIdProveedor() == $_GET["idproveedor"] && $vehiculoBorra -> getIdNombre() == $_GET["idnombre"] && $vehiculoBorra -> getModelo() == $_GET["modelo"]){
            $var=key($_SESSION["Productos"]);
            unset($_SESSION["Productos"][$var]);
        }
    }
}
if($confirmado){
    if($porcentaje==100){
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $venta =new Venta("", $_SESSION["id"],$fecha,$_GET["total"]);
    $venta -> insertarVenta();
    $venta -> consultar();
    $venta -> ingresarVehiculos($_SESSION["Productos"]);
    $_SESSION["Productos"]=array();
    $informacion="Se realizo la siguiente venta:<br>identificación: ". $venta -> getIdVenta(). "<br>cliente: ".$_SESSION["id"]."<br>Valor: ".$_GET["total"];
    $log = new Log("","3",$_SESSION["id"],"",$informacion,$fecha);
    $log -> insertarLogCliente();
    }else{ 
        $error=1
        ?>
        <script>
            alert("Usted debe completar la información del perfil para realizar una compra");
        </script>
    <?php }
}
$cantidad=sizeof($_SESSION["Productos"]);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-striped table-dark">
                    <thead>
                        <tr>
                        <th scope="col">Imagen</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Precio</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total=0;
                        foreach($_SESSION["Productos"] as $vehiculoActual){
                            $total=$total + $vehiculoActual -> getPrecio();
                            echo "<tr>";
                            echo "<td><img src='". $vehiculoActual -> getFoto() ."' alt='imagen' class='img-fluid max-width: 100% height: auto' width='100px'></td>";
                            echo "<td>". $vehiculoActual -> nombreProveedor() ."</td>";
                            echo "<td>". $vehiculoActual -> nombreVehiculo() ."</td>";
                            echo "<td>". $vehiculoActual -> getModelo() ."</td>";
                            echo "<td>". $vehiculoActual -> getPrecio() ."</td>";
                            echo "<td><a href='index.php?pid=". base64_encode("Presentacion/Cliente/carrito.php")."&idproveedor=". $vehiculoActual -> getIdProveedor() ."&idnombre=". $vehiculoActual -> getIdNombre() ."&modelo=". $vehiculoActual -> getModelo() ."&eliminado=true' data-toggle='tooltip' data-placement='top' title='Elimina este vehiculo del carrito'><i class='fas fa-times'></i></a></td>";
                            echo "</tr>";
                        }
                        ?>
                        <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th>
                        Total: 
                        </th>
                        <td>
                        <?php echo $total ?>
                        </td>
                        </tr>
                    </tbody>
                    
                </table>
            </div>
            <?php echo ($cantidad==0)?"<center><h1>Aun no has agregado productos al carrito</h1><center>":""?>
            <a href='index.php?pid=<?php echo base64_encode("Presentacion/Cliente/carrito.php") ?>&confirmado=true&total=<?php echo $total ?>' class='btn btn-info btn-lg btn-block mt-3 <?php echo ($cantidad==0)?"disabled":"" ?>' role='button' aria-pressed='false' >Confirmar compra</a>
            <?php
                if($error==1){
                    echo "<div class='alert alert-danger alert-dismissible fade show mt-3' role='alert'>
                    Usted debe completar la información de su perfil para confirmar una venta<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                    </button>
                    </div>";
                }else if($confirmado){
                                echo "<div class='alert alert-success alert-dismissible fade show mt-3' role='alert'>
                                La venta se ha registrado con exito<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                                </button>
                                </div>";
                } 
            ?>           
        </div>
    </div>
</div>
<script>
</script>