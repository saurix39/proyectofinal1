<?php
if(isset($_GET["idcliente"])){
date_default_timezone_set("America/Bogota");
$cliente=new Cliente($_GET["idcliente"]);
    if($cliente -> consultarEstado()==1){
        $cliente -> inhabilitar();
        $informacion="Se actualizo el estado del cliente:<br>identificación: ". $_GET["idcliente"] . "<br>administrador: ".$_GET["idadministrador"]."<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado";
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","4",$_GET["idadministrador"],"",$informacion,$fecha);
        $log -> insertarLogAdministrador();
        echo "<i class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></i>";
    }else{
        $cliente -> habilitar();
        $informacion="Se actualizo el estado del cliente:<br>identificación: ". $_GET["idcliente"] . "<br>administrador: ".$_GET["idadministrador"]."<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado";
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","4",$_GET["idadministrador"],"",$informacion,$fecha);
        $log -> insertarLogAdministrador();
        echo "<i class='fab fa-creative-commons-by' data-toggle='tooltip' data-placement='left' title='Habilitado'></i>";
    }    
}
?>
    


