<?php
$error=0;
$registrado=false;
if(isset($_POST["registrar"])){
$correo=$_POST["correo"];
$clave=$_POST["clave"];
$cliente= new Cliente("","","","",$correo,$clave);
if($cliente -> existeCorreo()){
    $error=1;
}else{
    $cliente -> registrarCliente();
    $cliente -> autenticar();
    $informacion="Se registro un cliente con la siguiente información:<br>correo: ". $correo."<br>Identificación: ". $cliente -> getIdCliente();
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $log=new Log("","1",$cliente -> getIdCliente(),"",$informacion,$fecha);
    $log -> insertarLogCliente();
    $registrado=true;
}
}
?>
<div class="container-fluid fomularioregistrar">
    <div class="row align-items-center vh-100">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="card-body text-white bg-dark mt-3 text-center">
                <h3>Formulario de registro</h3>
            </div>
            <div class="card-body text-white bg-dark mt-1">
                <form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/registrarCliente.php")?>" method="post">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="correo" class="form-control" aria-describedby="emailHelp" placeholder="Ejemplo@correo.com" required>
                                <small class="form-text text-muted">Tu email nunca sera comparido con otras personas.</small>
                            </div>
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input type="password" name="clave" class="form-control" placeholder="Micontraseña123." required>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar</button>
                            </div>
                            <?php if($error==1){
                                echo "<div class=\"alert alert-danger\" role=\"alert\">El correo ya fue registrado</div>";
                            }else if($registrado){
                                echo "<div class=\"alert alert-success\" role=\"alert\">Registro con exito <a href='index.php?pid=". base64_encode("Presentacion/formAutenticar.php") ."'>inicia sesión</a></div>";
                            } 
                            ?>
                </form>
            </div>
        </div>
    
    </div>
</div>