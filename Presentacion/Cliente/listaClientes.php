<?php
$cliente=new Cliente();
$cantidad=5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina=1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$clientes=$cliente -> lista($cantidad,$pagina);
$totalRegistros=$cliente -> consultarCantidad();
$totalPaginas=ceil($totalRegistros/$cantidad);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class= "card-body bg-dark mt-3 text-white">
            <center><h1>Lista de clientes</h1></center>
            </div>
            <div class="card-body bg-dark mt-1">
                <div class="table-responsive">
                    <table class="table table-striped table-dark">
                                <thead>
                                    <tr>
                                    <th scope="col">Identificacion del cliente</th>
                                    <th scope="col">Cedula del cliente</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellido</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Direccion</th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($clientes as $clienteActual){
                                        echo "<tr>";
                                        echo "<td>". $clienteActual -> getIdCliente() ."</td>";
                                        echo "<td>". $clienteActual -> getId() ."</td>";
                                        echo "<td>". $clienteActual -> getNombre() ."</td>";
                                        echo "<td>". $clienteActual -> getApellido() ."</td>";
                                        echo "<td>". $clienteActual -> getCorreo() ."</td>";
                                        echo "<td>". $clienteActual -> getDireccion() ."</td>";
                                        echo "<td><div id='icono". $clienteActual -> getIdCliente() ."'>". (($clienteActual -> getEstado()=="1")?"<i class='fab fa-creative-commons-by' data-toggle='tooltip' data-placement='left' title='Habilitado'></i>":"<i class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></i>"). "</div></td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>
                    </table>
                </div>
                <div class="mx-auto mt-3" style="width: 200px;">
                    <?php 
                        if($totalRegistros==0){
                            echo "<center><h1>No hay clientes para mostrar</h1></center>";
                        }else{ ?>
                            <nav>
                                <ul class="pagination">
                                    <li class="page-item <?php echo ($pagina==1)? "disabled":""; ?>"><a class="page-link" href="index.php?pid= <?php echo base64_encode("Presentacion/Cliente/listaClientes.php"). "&pagina=" . ($pagina-1) ."&cantidad=". $cantidad ?>"> &lt;&lt; </span></a></li>
                                    <?php for($i=1;$i<=$totalPaginas;$i++){
                                            if($pagina==$i){
                                                echo "<li style='z-index:1'class='page-item active' aria-current='page'><span class='page-link'> ". $i ."<span class='sr-only'></span></span></li>";
                                            }else{
                                                echo "<li class='page-item'><a class='page-link' href='index.php?pid=". base64_encode("Presentacion/Cliente/listaClientes.php") ."&pagina=". $i . "&cantidad=". $cantidad ."'>". $i ."</a></li>";
                                            }
                                        }	?>
                                    <li class="page-item <?php echo ($pagina==$totalPaginas)?"disabled":""; ?>"><a class="page-link" href="<?php echo "index.php?pid=". base64_encode("Presentacion/Cliente/listaClientes.php") ."&pagina=". ($pagina+1) . "&cantidad=". $cantidad?>"> &gt;&gt; </a></li>
                                    <li>
                                        <select id="cantidad">
                                            <option value="5" <?php echo ($cantidad==5)?"selected":"" ?>>5</option>
                                            <option value="10" <?php echo ($cantidad==10)?"selected":"" ?>>10</option>
                                            <option value="20" <?php echo ($cantidad==20)?"selected":"" ?>>20</option>
                                            <option value="50" <?php echo ($cantidad==50)?"selected":"" ?>>50</option>
                                        </select>
                                    </li>	
                                </ul>						
                            </nav>
                    <?php } ?>
			    </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    <?php
    foreach($clientes as $clienteActual){    
    ?>
        $("#icono<?php echo $clienteActual -> getIdCliente() ?>").click(function(e){
            $('[data-toggle="tooltip"]').tooltip('hide');
            var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Cliente/listaClientesAjax.php") ?>&idcliente=<?php echo $clienteActual -> getIdCliente() ?>&idadministrador=<?php echo $_SESSION["id"] ?>";
            $("#icono<?php echo $clienteActual -> getIdCliente() ?>").load(url);
        });
    <?php } ?>
});
$("#cantidad").on("change", function(){
	url="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/listaClientes.php")?>&cantidad="+ $(this).val();
	location.replace(url);
});
</script>

