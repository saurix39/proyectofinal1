<div class="container-fluid">
  <div class="row">
    <div class="col-md-1 col-0">
    </div>
    <div class="col-md-10 col-12">
      <div id="carouselExampleIndicators" class="carousel slide  carousel-fade mt-3" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active" data-interval="10000">
            <img src="Img/bmw-2.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h4>Bienvenido</h4>
              <p>En este sitio web encontraras vehiculos de gama alta.</p>
            </div>
          </div>
          <div class="carousel-item" data-interval="5000">
            <img src="Img/bmw-g05.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h4>Sin largos tramites</h4>
              <p>El proceso de venta en linea evita el papeleo excesivo.</p>
            </div>
          </div>
          <div class="carousel-item" data-interval="5000">
            <img src="Img/bmw-7series.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h4>Obten el tuyo</h4>
              <p>Registrese y obtenga informacion de nuestros vehiculos.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
  <div class="row mt-3">
  <div class="col text-center">
    <br>
    <h3>Nuestras instalaciones</h3>
    <p>
    Poseemos con las mejores instalaciones de la ciudad, con la finalidad de que te sientas a gusto
    </p>
    <h6>Visitanos!</h6>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-md-8 col-12 mt-1">
      <img src="Img/instalaciones1.jpg" width="100%" alt="">
    </div>
    <div class="col-md-4 col-12 mt-1 text-center">
      <img src="Img/instalaciones8.jpg" width="100%" alt="">
    </div>
  </div>
</div>
