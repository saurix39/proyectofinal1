<div class="container-fluid autenticar">
    <div class="row align-items-center vh-100">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="card-body text-white bg-dark mt-3 text-center">
                <h3>Autenticate</h3>
            </div>
            <div class="card-body text-white bg-dark mt-1">
                <form action="index.php?pid=<?php echo base64_encode("Presentacion/Autenticar.php")?>" method="post">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="correo" class="form-control" aria-describedby="emailHelp" placeholder="Ejemplo@correo.com" required>
                                <small class="form-text text-muted">Tu email nunca sera comparido con otras personas.</small>
                            </div>
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input type="password" name="clave" class="form-control" placeholder="Micontraseña123." required>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="ingresar" class="form-control btn btn-secondary">Ingresar</button>
                            </div>
                            <?php if(isset($_GET["error"]) && $_GET["error"]==1){
                                echo "<div class=\"alert alert-danger\" role=\"alert\">Error de correo o clave</div>";
                            }else if(isset($_GET["error"]) && $_GET["error"]==2){
                                echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";
                            }else if(isset($_GET["error"]) && $_GET["error"]==3){
                                echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
                            } 
                            ?>
                            <p>Eres nuevo? <a href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/registrarCliente.php")?>">Registrate aquí</a></p>
                </form>
            </div>
        </div>
    
    </div>
</div>