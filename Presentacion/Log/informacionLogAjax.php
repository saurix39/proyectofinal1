<?php
    $idactor=$_GET["idactor"];
    $idlog=$_GET["idlog"];
    $idtipo=$_GET["idtipo"];
    $log = new Log($idlog,"",$idactor,$idtipo);
    $log -> consultarInfo();
    if($idtipo==1){
        $actor=new Administrador($idactor);
    }else if($idtipo==2){
        $actor=new Cliente($idactor);
    }else{
        $actor=new Proveedor($idactor);
    }
    $actor -> consultar();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-0">
        </div>
        <div class="col-md-8 col-12">
            <center><h3>Actor: <?php echo (($log -> getIdTipo()==1)?"Administrador":(($log -> getIdTipo()==2)?"Cliente":"Proveedor")) ?></h3></center>
            <center><h3>Acción: <?php echo $log -> nombreAccion() ?></h3></center>
            <img class="rounded" src="<?php echo ($actor -> getFoto() !="")?$actor -> getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/flatastic-4/512/User-blue-icon.png" ?>" alt="" width="100%">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                    <th scope="row">Identificación</th>
                    <td><?php echo (($log -> getIdTipo()==1)?$actor -> getIdAdministrador():(($log -> getIdTipo()==2)?$actor -> getIdCliente():$actor -> getIdProveedor()))?></td>
                    </tr>
                    <?php if($log -> getIdTipo()==2){ ?>
                        <tr>
                        <th scope="row">Cedula</th>
                        <td><?php echo $actor -> getId() ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                    <th scope="row">Nombre</th>
                    <td><?php echo $actor -> getNombre() ?></td>
                    </tr>
                    <?php if($log -> getIdTipo()==1 || $log -> getIdTipo()==2){ ?>
                    <tr>
                    <th scope="row">Apellido</th>
                    <td><?php echo $actor -> getApellido() ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                    <th scope="row">Correo</th>
                    <td><?php echo $actor -> getCorreo() ?></td>
                    </tr>
                    <?php if($log -> getIdTipo()==3){ ?>
                    <tr>
                    <th scope="row">Nit</th>
                    <td><?php echo $actor -> getNit() ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                    <th scope="row">Fecha de la acción</th>
                    <td><?php echo $log -> getFecha() ?></td>
                    </tr>
                    <tr>
                    <th scope="row">Hora de la acción</th>
                    <td><?php echo $log -> getHora() ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <center><H3>Información</H3></center>
            <?php echo $log -> getInformacion() ?>
        </div>
    </div>
</div>