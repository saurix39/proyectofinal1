<div style="display:none; overflow-y: scroll;" id="modalSup">
    <div class="container-fluid" id="modalInfo">
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body bg-dark mt-2">
                <input id="filtro" class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Search">
                <div id="resultados">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Log/logEdicionAjax.php") ?>";
    $("#resultados").load(url);
    $("#filtro").keyup(function() {    
	    	var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Log/logEdicionAjax.php") ?>&filtro=" + btoa($(this).val()); 
    		$("#resultados").load(url);
    });
});
</script>