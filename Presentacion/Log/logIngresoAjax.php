<?php
$filtro="";
if(isset($_GET["filtro"])){
$filtro=base64_decode($_GET["filtro"]);
}
$log = new Log();
$logs = $log -> consultarLogIngreso($filtro);
function ordenar($a, $b){
    return (strtotime($a -> getFecha()) - strtotime($b -> getFecha()));
}
usort($logs,'ordenar');
$logs=array_reverse($logs);
?>
<div class="table-responsive">
    <table class="table table-striped table-dark">
        <thead>
            <tr>
            <th scope="col">Acción</th>
            <th scope="col">Actor</th>
            <th scope="col">Fecha</th>
            <th scope="col">Hora</th>
            <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $total=0;
            foreach($logs as $logActual){
                echo "<tr>";
                echo "<td>Ingreso</td>";
                echo "<td>".(($logActual -> getIdTipo()==1)?"Administrador-":(($logActual -> getIdTipo()==2)?"Cliente - ":"Proveedor - ")). $logActual -> getIdActor() ."</td>";
                echo "<td>". $logActual -> getFecha() ."</td>";
                echo "<td>". $logActual -> getHora() ."</td>";
                echo "<td><a id='modal". $logActual -> getIdLog() ."-". $logActual -> getIdTipo() ."-". $logActual -> getIdActor() ."' data-toggle='tooltip' data-placement='top' title='Informacion del log'><i class='fas fa-info-circle'></i></a></td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>
<script>
$(document).ready(function(){
    <?php
    foreach($logs as $logActual){    
    ?>
        $("#modal<?php echo $logActual -> getIdLog() ."-". $logActual -> getIdTipo() ."-". $logActual -> getIdActor() ?>").click(function(e){
            var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Log/informacionLogAjax.php") ?>&idlog=<?php echo $logActual -> getIdLog() ?>&idtipo=<?php echo $logActual -> getIdTipo() ?>&idactor=<?php echo $logActual -> getIdActor() ?>";
            $("#modalInfo").load(url);
            $("#modalSup").modal();
        });
    <?php } ?>
});
</script>