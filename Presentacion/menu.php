<div class="container-fluid">
  <div class="row">
    <div class="col">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php?">
          <img src="Img/iconoP.png" width="30" height="30" class="d-inline-block align-top rounded" alt="" loading="lazy">
          <strong>BoostingCo</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php echo ($pos=="sobreNosotros")? "active":"" ?>">
              <a class="nav-link" href="index.php?pid='<?php echo base64_encode("Presentacion/sobreNosotros.php")?>'&pos=sobreNosotros">Sobre nosotros</a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php echo ($pos=="servicios")? "active":"" ?>" href="index.php?pid='<?php echo base64_encode("Presentacion/Servicios.php")?>'&pos=servicios">Servicios</a>
            </li>
          </ul calss="navbar-nav">
          <a href="index.php?pid='<?php echo base64_encode("Presentacion/formAutenticar.php") ?>'" class="btn btn-outline-info my-2 my-sm-0" role="button" aria-pressed="false">Inicia sesión</a>
        </div>
      </nav>
    </div>
  </div>
</div>