<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body cataf mt-2">
                <div class="row">
                    <div class="col mt-3">
                        <input id="filtro" class="form-control mr-sm-2" type="search" placeholder="Buscar..." aria-label="Search">
                    </div>
                </div>
                <div id="resultados" class="row mt-3">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/catalogoAjax.php") ?>&filtro=";
    $("#resultados").load(url);
    $("#filtro").keyup(function() {    
	    	var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/catalogoAjax.php") ?>&filtro=" + btoa($(this).val()); 
    		$("#resultados").load(url);
    });
});
</script>
