<?php
$vehiculo=new Vehiculo("","",$_SESSION["id"]);
$cantidad=5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina=1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$vehiculos=$vehiculo -> listaProv($cantidad,$pagina);
$totalRegistros=$vehiculo -> consultarCantidadProv();
$totalPaginas=ceil($totalRegistros/$cantidad);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class= "card-body bg-dark mt-3 text-white">
            <center><h1>Lista de vehiculos</h1></center>
            </div>
            <div class="card-body bg-dark mt-1">
                <div class="table-responsive">
                    <table class="table table-striped table-dark">
                                <thead>
                                    <tr>
                                    <th scope="col">Identificacion del vehiculo</th>
                                    <th scope="col">Identificacion de fabrica</th>
                                    <th scope="col">Proveedor</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Modelo</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total=0;
                                    foreach($vehiculos as $vehiculoActual){
                                        echo "<tr>";
                                        echo "<td>". $vehiculoActual -> getIdvehiculo() ."</td>";
                                        echo "<td>". $vehiculoActual -> getIdFabrica() ."</td>";
                                        echo "<td>". $vehiculoActual -> nombreProveedor() ."</td>";
                                        echo "<td>". $vehiculoActual -> nombreVehiculo() ."</td>";
                                        echo "<td>". $vehiculoActual -> getModelo() ."</td>";
                                        echo "<td>". $vehiculoActual -> getPrecio() ."</td>";
                                        echo "<td><div id='icono". $vehiculoActual -> getIdvehiculo() ."'>". (($vehiculoActual -> getEstado()=="1")?"<i class='fas fa-car' data-toggle='tooltip' data-placement='left' title='Habilitado'></i>":"<i class='far fa-times-circle' data-toggle='tooltip' data-placement='left' title='Vendido'></i>")."</div></td>";
                                        echo "<td><a href='index.php?pid=". base64_encode("Presentacion/Vehiculo/editarVehiculo.php") ."&idvehiculo=". $vehiculoActual -> getIdvehiculo() ."'><i class='fas fa-edit' data-toggle='tooltip' data-placement='left' title='Editar vehiculo'></i></a></td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>
                    </table>
                </div>
                <div class="mx-auto mt-3" style="width: 200px;">
                    <?php 
                        if($totalRegistros==0){
                            echo "<center><h1>No hay administradores para mostrar</h1></center>";
                        }else{ ?>
                            <nav>
                                <ul class="pagination">
                                    <li class="page-item <?php echo ($pagina==1)? "disabled":""; ?>"><a class="page-link" href="index.php?pid= <?php echo base64_encode("Presentacion/Vehiculo/listaVehiculosProv.php"). "&pagina=" . ($pagina-1) ."&cantidad=". $cantidad ?>"> &lt;&lt; </span></a></li>
                                    <?php for($i=1;$i<=$totalPaginas;$i++){
                                            if($pagina==$i){
                                                echo "<li style='z-index:1'class='page-item active' aria-current='page'><span class='page-link'> ". $i ."<span class='sr-only'></span></span></li>";
                                            }else{
                                                echo "<li class='page-item'><a class='page-link' href='index.php?pid=". base64_encode("Presentacion/Vehiculo/listaVehiculosProv.php") ."&pagina=". $i . "&cantidad=". $cantidad ."'>". $i ."</a></li>";
                                            }
                                        }	?>
                                    <li class="page-item <?php echo ($pagina==$totalPaginas)?"disabled":""; ?>"><a class="page-link" href="<?php echo "index.php?pid=". base64_encode("Presentacion/Vehiculo/listaVehiculosProv.php") ."&pagina=". ($pagina+1) . "&cantidad=". $cantidad?>"> &gt;&gt; </a></li>
                                    <li>
                                        <select id="cantidad">
                                            <option value="5" <?php echo ($cantidad==5)?"selected":"" ?>>5</option>
                                            <option value="10" <?php echo ($cantidad==10)?"selected":"" ?>>10</option>
                                            <option value="20" <?php echo ($cantidad==20)?"selected":"" ?>>20</option>
                                            <option value="50" <?php echo ($cantidad==50)?"selected":"" ?>>50</option>
                                        </select>
                                    </li>	
                                </ul>						
                            </nav>
                    <?php } ?>
			    </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    <?php
    foreach($vehiculos as $vehiculoActual){    
    ?>
        $("#icono<?php echo $vehiculoActual -> getIdVehiculo() ?>").click(function(e){
            $('[data-toggle="tooltip"]').tooltip('hide');
            var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/listaVehiculosAjax.php") ?>&idvehiculo=<?php echo $vehiculoActual -> getIdVehiculo() ?>&idproveedor=<?php echo $_SESSION["id"] ?>";
            $("#icono<?php echo $vehiculoActual -> getIdVehiculo() ?>").load(url);
        });
    <?php } ?>
});
$("#cantidad").on("change", function(){
	url="index.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/listaVehiculosProv.php")?>&cantidad="+ $(this).val();
	location.replace(url);
});
</script>