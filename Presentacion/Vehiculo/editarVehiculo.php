<?php
$idvehiculo=$_GET["idvehiculo"];
$proveedor = new Proveedor();
$nombres = $proveedor -> obtenerNombres();
if(isset($_POST["editar"])){
    $idNombre=$proveedor -> consultarIdNombre($_POST["nombre"]);
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Vehiculo/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $vehiculo = new Vehiculo($idvehiculo);
        $vehiculo -> consultarEd();
        if($vehiculo -> getFoto()!=""){
            unlink($vehiculo -> getFoto());
        }
        $vehiculo= new Vehiculo($idvehiculo,$_POST["idFabrica"],$_SESSION["id"],$idNombre,"",$_POST["modelo"],$_POST["precio"],$rutaRemota);
        $vehiculo -> editarVehiculo();
        $informacion="se edito el siguiente vehiculo:<br>identificacion: ". $idvehiculo ."<br>Se ingresaron los siguientes datos:<br>identificación de fabrica: ". $_POST["idFabrica"] ."<br>identificación del proveedor: ".$_SESSION["id"]."<br>identificación del nombre: ".$idNombre."<br>modelo: ".$_POST["modelo"]."<br>precio: ".$_POST["precio"];
        date_default_timezone_set("America/Bogota");
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","5",$_SESSION["id"],"",$informacion,$fecha);
        $log -> insertarLogProveedor();
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body bg-dark mt-1">
                <div class="row">
                    <div class="col-md-2 col-0"></div>
                    <div class="col-md-8 col-12">
                        <div class="card-header border border-white">
                            <center><h1 class="text-white">Formulario de Edición</h1></center>
                        </div>
                        <div class="card-body border border-white">
                            <form action="index.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/editarVehiculo.php")?>&idvehiculo=<?php echo $idvehiculo ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <h4 class="text-white">Identificacion de fabrica</h4>  
                                    <input type="text" name="idFabrica" class="form-control" required>
                                </div>
                                <h4 class="text-white">Nombre del vehiculo</h4>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                                    </div>
                                    <select class="custom-select" name="nombre" required>
                                        <option selected>Escoger...</option>
                                        <?php
                                        foreach($nombres as $nombreactual){
                                            echo "<option value='". $nombreactual ."'>". $nombreactual ."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <h4 class="text-white">Modelo</h4>  
                                    <input type="text" name="modelo" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <h4 class="text-white">Precio</h4>  
                                    <input type="text" name="precio" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <h4 class="text-white">Imagen</h4> 
                                    <input type="file" name="imagen" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="editar" class="form-control btn btn-secondary">Editar vehiculo</button>
                                </div>
                                <?php if(isset( $_POST["editar"])){?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Datos ingresados con exito!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>