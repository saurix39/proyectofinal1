<?php
$vehiculo=new Vehiculo("","",$_GET["idproveedor"],$_GET["idnombre"],"",$_GET["modelo"]);
$vehiculo -> consultar();
$añadido=false;
if(isset($_GET["añadido"])){
$añadido=$_GET["añadido"];
array_push($_SESSION["Productos"],$vehiculo);
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-1 "></div>
        <div class="col-md-8 col-10">
            <div class="row">
                <div class="col  card-body bg-dark mt-3">
                    <div class="row">
                        <div class="col-md-3 col-12">
                            <img src="<?php echo $vehiculo -> getFoto() ?>" alt="" width="100%">
                        </div>
                        <div class="col-md-9 col-12">
                            <table class="table table-striped table-dark">
                                <tr>
                                <th scope="col">Marca</th>
                                <td><?php echo ($vehiculo -> nombreProveedor()!=null)?$vehiculo -> nombreProveedor():"Sin información" ?></td>
                                </tr>
                                <tr>
                                <th scope="col">Nombre</th>
                                <td><?php echo ($vehiculo -> nombreVehiculo()!="")?$vehiculo -> nombreVehiculo():"Sin información"?></td>
                                </tr>
                                <tr>
                                <th scope="col">Modelo</th>
                                <td><?php echo ($vehiculo -> getModelo()!="")?$vehiculo -> getModelo():"Sin información"?></td>
                                </tr>
                                <tr>
                                <th scope="col">Precio</th>
                                <td><?php echo ($vehiculo -> getPrecio()!="")?$vehiculo -> getPrecio():"Sin información"?></td>
                                </tr>        
                            </table>
                        </div>
                        <a href='index.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/informacionVehiculo.php") ?>&idproveedor=<?php echo $vehiculo -> getIdProveedor() ?>&idnombre=<?php echo $vehiculo -> getIdNombre() ?>&modelo=<?php echo $vehiculo -> getModelo() ?>&añadido=true' class='btn btn-info btn-lg btn-block mt-3 <?php echo ($añadido)?"disabled":"" ?>' role='button' aria-pressed='false' >Añadir al carrito de compras</a>
                    </div>
                    <?php 
                            if($añadido){
                                echo "<div class='alert alert-success alert-dismissible fade show mt-3' role='alert'>
                                El vehiculo ha sido añadido al carrito con exito<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                                </button>
                                </div>";
                            }
                        ?>
                </div>
            </div>
        </div>
    </div>
</div>