<?php
if(isset($_GET["idvehiculo"])){
date_default_timezone_set("America/Bogota");
$vehiculo=new Vehiculo($_GET["idvehiculo"]);
    if($vehiculo -> consultarEstado()==1){
        $vehiculo -> inhabilitar();
        if(isset($_GET["idadministrador"])){
            $informacion="Se actualizo el estado del vehiculo:<br>identificación: ". $_GET["idvehiculo"] . "<br>administrador: ".$_GET["idadministrador"]."<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado";
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","4",$_GET["idadministrador"],"",$informacion,$fecha);
            $log -> insertarLogAdministrador();
        }else{
            $informacion="Se actualizo el estado del vehiculo:<br>identificación: ". $_GET["idvehiculo"] . "<br>proveedor: ".$_GET["idproveedor"]."<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado";
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","4",$_GET["idproveedor"],"",$informacion,$fecha);
            $log -> insertarLogProveedor();
        }
        echo "<i class='far fa-times-circle' data-toggle='tooltip' data-placement='left' title='Vendido'></i>";
    }else{
        $vehiculo -> habilitar();
        if(isset($_GET["idadministrador"])){
            $informacion="Se actualizo el estado del vehiculo:<br>identificación: ". $_GET["idvehiculo"] . "<br>administrador: ".$_GET["idadministrador"]."<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado";
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","4",$_GET["idadministrador"],"",$informacion,$fecha);
            $log -> insertarLogAdministrador();
        }else{
            $informacion="Se actualizo el estado del vehiculo:<br>identificación: ". $_GET["idvehiculo"] . "<br>proveedor: ".$_GET["idproveedor"]."<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado";
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","4",$_GET["idproveedor"],"",$informacion,$fecha);
            $log -> insertarLogProveedor();
        }
        echo "<i class='fas fa-car' data-toggle='tooltip' data-placement='left' title='Habilitado'></i>";
    }
}
?>