<?php
$filtro=base64_decode($_GET["filtro"]);
$vehiculo=new Vehiculo();
$vehiculos = $vehiculo -> consultarFiltro($filtro);
$totalRegistros=$vehiculo -> consultarCantidadFiltro($filtro);	
?>
<div class="col">
    <?php
        if($totalRegistros==0){
            echo "<center><h1 class='text-white'>No hay vehiculos para mostrar</h1></center>";
        }
        echo "<div class='row'>";
            foreach($vehiculos as $vehiculoActual){
                echo "<div class='col-md-3 col-12 text-center'>";
                    echo "<div class='card-body mt-2 text-white'>";
                        echo "<h5>". $vehiculoActual -> nombreProveedor() ." - ". $vehiculoActual -> nombreVehiculo() ." - ". $vehiculoActual -> getModelo() ."</h5>";
                        echo "<div class='catalogo'>";
                        echo "<a href='index.php?pid=". base64_encode("Presentacion/Vehiculo/informacionVehiculo.php") ."&idproveedor=". $vehiculoActual -> getIdProveedor() ."&idnombre=". $vehiculoActual -> getIdNombre() ."&modelo=". $vehiculoActual -> getModelo() ."'><img src='". $vehiculoActual -> getFoto() ."' alt='imagen' width='100%'></a>";
                        echo "</div>";
                        echo "<a href='index.php?pid=". base64_encode("Presentacion/Vehiculo/informacionVehiculo.php") ."&idproveedor=". $vehiculoActual -> getIdProveedor() ."&idnombre=". $vehiculoActual -> getIdNombre() ."&modelo=". $vehiculoActual -> getModelo() ."' class='btn btn-secondary btn-lg btn-block mt-3' role='button' aria-pressed='false'>Ver información</a>";
                    echo "</div>";
                echo "</div>";
            }
         echo "</div>";
    ?>
</div>