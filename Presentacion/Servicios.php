<div class="container">
    <div class="row">
        <div class="col">
            <div class="card-body text-white bg-dark mt-3 text-center">
                <h2>Nuesto servicio</h2>
            </div>
            <div class="card-body text-white bg-dark mt-1 text-center">
                <h2>Bienvenido a BoostingCO</h2>
                <br>
                <p>Te invitamos a probar nuestros servicio de venta de vehiculos.
                    Para poder acceder a la informacion de nuestros veiculos y 
                    saber como adquirir uno debes registrarte en nuestra plataforma.
                    Si no deseas adquirir un vehiculo pero aun asi quieres acceder a nuestro catalogo, tambien 
                    debes hacer uso de una cuenta registrada en nuestra plataforma.     
                </p>
                <br>
                <a href="index.php?pid='<?php echo base64_encode("Presentacion/Cliente/registrarCliente.php") ?>'" class="btn btn-outline-info my-2 my-sm-0" role="button" aria-pressed="false">Adquiere una cuenta</a>
            </div>
        </div>
    </div>
</div>