<?php
if(isset($_GET["idventa"])){
    $venta=new Venta($_GET["idventa"]);
    $venta -> consultarInformacion();
    $cliente = new Cliente($venta -> getIdCliente());
    $cliente -> consultar();
    $vehiculos=$venta -> consultarVehiculosVenta();
}
?>
<center><h3>Información de la factura</h3></center>
<div class="row">
    <div class="col-md-2 col-0">
    </div>
    <div class="col-md-8 col-12">
        <img class="rounded" src="<?php echo ($cliente -> getFoto()!="")?$cliente -> getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/flatastic-4/512/User-blue-icon.png"?>" alt="imagen del cliente" width="100%">
    </div>
</div>
<div class="row">
    <div class="col">
        <table class="table table-bordered">
            <tbody>
                <tr>
                <th scope="row">Identificación</th>
                <td><?php echo $cliente -> getIdCliente() ?></td>
                </tr>
                <tr>
                <th scope="row">Cedula</th>
                <td><?php echo $cliente -> getId() ?></td>
                </tr>
                <tr>
                <th scope="row">Nombre</th>
                <td><?php echo $cliente -> getNombre() ?></td>
                </tr>
                <tr>
                <th scope="row">Apellido</th>
                <td><?php echo $cliente -> getApellido() ?></td>
                </tr>
                <tr>
                <th scope="row">Fecha de la venta</th>
                <td><?php echo $venta -> getFecha() ?></td>
                </tr>
                <tr>
                <th scope="row">Precio de la venta</th>
                <td><?php echo $venta -> getValorVenta() ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<center><h3>Vehiculos de la factura</h3></center>
<div class="row">
    <div class="col">
        <table class="table table-bordered">
            <tbody>
            <?php foreach($vehiculos as $vehiculoActual) { ?>
                <tr>
                <th scope="row">Identificación del vehiculo</th>
                <td><?php echo $vehiculoActual -> getIdVehiculo() ?></td>
                </tr>
                <tr>
                <th scope="row">Proveedor</th>
                <td><?php echo $vehiculoActual-> nombreProveedor() ?></td>
                </tr>
                <tr>
                <th scope="row">Nombre</th>
                <td><?php echo $vehiculoActual -> nombreVehiculo() ?></td>
                </tr>
                <tr>
                <th scope="row">Precio</th>
                <td><?php echo $vehiculoActual -> getPrecio() ?></td>
                </tr>
            <?php } ?>    
            </tbody>
        </table>
    </div>
</div>