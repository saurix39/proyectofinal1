<?php
if(isset($_GET["idadministrador"])){
    date_default_timezone_set("America/Bogota");
    $administrador=new Administrador($_GET["idadministrador"]);
    if($administrador -> consultarEstado()==1){
        $administrador -> inhabilitar();
        $informacion="Se actualizo el estado del Administrador:<br>identificación: ". $_GET["idadministrador"] . "<br>administrador: ".$_GET["idadautor"]."<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado";
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","4",$_GET["idadautor"],"",$informacion,$fecha);
        $log -> insertarLogAdministrador();
        echo "<i class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></i>";
    }else{
        $administrador -> habilitar();
        $informacion="Se actualizo el estado del Administrador:<br>identificación: ". $_GET["idadministrador"] . "<br>administrador: ".$_GET["idadautor"]."<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado";
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","4",$_GET["idadautor"],"",$informacion,$fecha);
        $log -> insertarLogAdministrador();
        echo "<i class='fab fa-creative-commons-by' data-toggle='tooltip' data-placement='left' title='Habilitado'></i>";
    }
}
?>
    
