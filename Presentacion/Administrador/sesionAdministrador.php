<?php
$administrador=new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card-header text-white bg-dark mt-2 text-center">
                <h2>Bienvenido administrador</h2>
            </div>
            <div class="card-body text-white bg-dark mt-1">
                <div class="row">
                    <div class="col-3">
                        <img src="<?php echo ($administrador -> getFoto()!="")?$administrador -> getFoto():"http://icons.iconarchive.com/icons/custom-icon-design/flatastic-4/512/User-blue-icon.png";?>" alt="Imagen del administrador" width="100%">
                    </div>
                    <div class="col-9">
                    <table class="table table-striped table-dark">
                            <tr>
                            <th scope="col">Nombre</th>
                            <td><?php echo $administrador -> getNombre()?></td>
                            </tr>
                            <tr>
                            <th scope="col">Apellido</th>
                            <td><?php echo $administrador -> getApellido()?></td>
                            </tr>
                            <tr>
                            <th scope="col">Correo</th>
                            <td><?php echo $administrador -> getCorreo()?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>