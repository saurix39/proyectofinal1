<?php
$venta = new Venta("","","","");
$cantidad=5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina=1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$ventas = $venta -> consultarVentas($cantidad,$pagina);
$totalRegistros=$venta -> consultarCantidad();
$totalPaginas=ceil($totalRegistros/$cantidad);
?>
<div style="display:none; overflow-y: scroll;" id="modalSup">
    <div class="container-fluid" id="modalInfo">
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body bg-dark mt-2">
                <div class="table-responsive">
                    <table class="table table-striped table-dark">
                                <thead>
                                    <tr>
                                    <th scope="col">Identificacion de la factura</th>
                                    <th scope="col">Identificacion del cliente</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Valor total</th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total=0;
                                    foreach($ventas as $ventaActual){
                                        echo "<tr>";
                                        echo "<td>". $ventaActual -> getIdVenta() ."</td>";
                                        echo "<td>". $ventaActual -> getIdCliente() ."</td>";
                                        echo "<td>". $ventaActual -> getFecha() ."</td>";
                                        echo "<td>". $ventaActual -> getValorVenta() ."</td>";
                                        echo "<td><a id='modal". $ventaActual -> getIdVenta() ."' data-toggle='tooltip' data-placement='top' title='Información de la factura'><i class='fas fa-info-circle'></i></a></td>"; 
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>
                    </table>
                </div>
                <div class="mx-auto" style="width: 200px;">
                    <?php 
                    if($totalRegistros==0){
                        echo "<center><h1>No hay facturas para mostrar</h1></center>";
                    }else{ ?>
        				<nav>
        					<ul class="pagination">
        						<li class="page-item <?php echo ($pagina==1)? "disabled":""; ?>"><a class="page-link" href="index.php?pid= <?php echo base64_encode("Presentacion/Administrador/ventas.php"). "&pagina=" . ($pagina-1) ."&cantidad=". $cantidad ?>"> &lt;&lt; </span></a></li>
								<?php for($i=1;$i<=$totalPaginas;$i++){
										if($pagina==$i){
											echo "<li style='z-index:1'class='page-item active' aria-current='page'><span class='page-link'> ". $i ."<span class='sr-only'></span></span></li>";
										}else{
											echo "<li class='page-item'><a class='page-link' href='index.php?pid=". base64_encode("Presentacion/Administrador/ventas.php") ."&pagina=". $i . "&cantidad=". $cantidad ."'>". $i ."</a></li>";
										}
									}	?>
        						<li class="page-item <?php echo ($pagina==$totalPaginas)?"disabled":""; ?>"><a class="page-link" href="<?php echo "index.php?pid=". base64_encode("Presentacion/Administrador/ventas.php") ."&pagina=". ($pagina+1) . "&cantidad=". $cantidad?>"> &gt;&gt; </a></li>
								<li>
									<select id="cantidad">
  										<option value="5" <?php echo ($cantidad==5)?"selected":"" ?>>5</option>
  										<option value="10" <?php echo ($cantidad==10)?"selected":"" ?>>10</option>
  										<option value="20" <?php echo ($cantidad==20)?"selected":"" ?>>20</option>
 										<option value="50" <?php echo ($cantidad==50)?"selected":"" ?>>50</option>
									</select>
								</li>	
							</ul>						
        				</nav>
                        <?php } ?>
				</div>
            </div>    
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    <?php
    foreach($ventas as $ventaActual){    
    ?>
        $("#modal<?php echo $ventaActual -> getIdVenta()?>").click(function(e){
            var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/informacionVentaAjax.php") ?>&idventa=<?php echo $ventaActual -> getIdVenta() ?>";
            $("#modalInfo").load(url);
            $("#modalSup").modal();
        });
    <?php } ?>
});
$("#cantidad").on("change", function(){
	url="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/ventas.php")?>&cantidad="+ $(this).val();
	location.replace(url);
});
</script>