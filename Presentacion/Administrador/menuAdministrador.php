<?php
$administrador= new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/sesionAdministrador.php")?>">
          <img src="Img/iconoP.png" width="30" height="30" class="d-inline-block align-top rounded" alt="" loading="lazy">
          <strong>BoostingCo</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Clientes
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/listaClientes.php") ?>">Lista</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Proveedores
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/registrarProveedor.php")?>">Registrar</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/listaProveedores.php") ?>">Lista</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Vehiculos
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/listaVehiculos.php") ?>">Lista</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Administradores
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/registrarAdministrador.php") ?>">Registrar</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/listaAdministradores.php") ?>">Lista</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ventas
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/ventas.php") ?>">Lista</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Log
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Log/logRegistro.php") ?>">Registro</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Log/logIngreso.php") ?>">Ingreso</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Log/logVenta.php") ?>">Venta</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Log/logEstado.php") ?>">Cambio de estado</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Log/logEdicion.php") ?>">Edicion</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Administrador: <?php echo $administrador -> getNombre()." ".$administrador -> getApellido()?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Editar perfil</a>
                <a class="dropdown-item" href="#">Editar foto</a>
                <a class="dropdown-item" href="#">Cambiar clave</a>
              </div>
            </li>
            <form class="form-inline" action="index.php?cerrarSesion=1" method="post">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Cerrar sesión</button>
            </form> 
          </ul>
        </div>
      </nav>
    </div>
  </div>
</div>