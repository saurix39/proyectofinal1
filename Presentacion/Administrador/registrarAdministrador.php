<?php
$error=0;
$registrado=false;
if(isset($_POST["registrar"])){
    date_default_timezone_set("America/Bogota");
    $nombre=$_POST["nombre"];
    $apellido=$_POST["apellido"];
    $correo=$_POST["correo"];
    $clave=$_POST["clave"];
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Administrador/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $administrador = new Administrador("",$nombre,$apellido,$correo,$clave,$rutaRemota);
        if($administrador -> existeCorreo()){
            $error=1;
        }else{
            $administrador -> registrarAdministrador();
            $administrador -> autenticar();
            $informacion="Se registro un Administrador con la siguiente información:<br>identificación: ". $administrador -> getIdAdministrador() ."<br>nombre: ". $nombre ."<br>apellido: ". $apellido ."<br>correo: ". $correo;
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
            $log -> insertarLogAdministrador();
            $registrado=true;
        }
    }else{
        $administrador = new Administrador("",$nombre,$apellido,$correo,$clave);
        if($administrador -> existeCorreo()){
            $error=1;
        }else{
            $administrador -> registrarAdministrador();
            $administrador -> autenticar();
            $informacion="Se registro un Administrador con la siguiente información:<br>identificación: ". $administrador -> getIdAdministrador() ."<br>nombre: ". $nombre ."<br>apellido: ". $apellido ."<br>correo: ". $correo;
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
            $log -> insertarLogAdministrador();
            $registrado=true;
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body bg-dark mt-1">
                <div class="row">
                    <div class="col-md-2 col-0">
                    </div>
                    <div class="col-md-8 col-12">
                        <div class="card-header border border-white text-white">
                            <center><h1>Registrar un administrador</h1></center>
                        </div>
                        <div class="card-body border border-white mt-1">
                            <form action="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/registrarAdministrador.php")?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="text-white">Nombre</label>  
                                    <input type="text" name="nombre" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Apellido</label>  
                                    <input type="text" name="apellido" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Correo</label>  
                                    <input type="email" name="correo" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Clave</label>  
                                    <input type="password" name="clave" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="text-white">Imagen</label> 
                                    <input type="file" name="imagen" class="form-control">
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar administrador</button>
                                </div>
                                <?php if($registrado){ ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Datos ingresados con exito!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php }else if($error==1){ ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        Este correo ya fue registrado!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php } ?>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>