<?php
$proveedor = new Proveedor();
$nombres = $proveedor -> obtenerNombres();
if(isset($_POST["registrar"])){
    $idNombre=$proveedor -> consultarIdNombre($_POST["nombre"]);
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Vehiculo/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $vehiculo = new Vehiculo("",$_POST["idFabrica"],$_SESSION["id"],$idNombre,"",$_POST["modelo"],$_POST["precio"],$rutaRemota);
        $vehiculo -> registrarVehiculo();
        $informacion="se registro el siguiente vehiculo:<br>identificación de fabrica: ". $_POST["idFabrica"] ."<br>identificación del proveedor: ".$_SESSION["id"]."<br>identificación del nombre: ".$idNombre."<br>modelo: ".$_POST["modelo"]."<br>precio: ".$_POST["precio"];
        date_default_timezone_set("America/Bogota");
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
        $log -> insertarLogProveedor();
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body bg-dark mt-1">
                <div class="row">
                    <div class="col-md-2 col-0"></div>
                    <div class="col-md-8 col-12">
                        <div class="card-header border border-white">
                            <center><h1 class="text-white">Registra un vehiculo</h1></center>
                        </div>
                        <div class="card-body border border-white">
                            <form action="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/registrarVehiculo.php")?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <h4 class="text-white">Identificacion de fabrica</h4>  
                                    <input type="text" name="idFabrica" class="form-control" required>
                                </div>
                                <h4 class="text-white">Nombre del vehiculo</h4>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                                    </div>
                                    <select class="custom-select" name="nombre" required>
                                        <option selected>Escoger...</option>
                                        <?php
                                        foreach($nombres as $nombreactual){
                                            echo "<option value='". $nombreactual ."'>". $nombreactual ."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <h4 class="text-white">Modelo</h4>  
                                    <input type="text" name="modelo" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <h4 class="text-white">Precio</h4>  
                                    <input type="text" name="precio" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <h4 class="text-white">Imagen</h4> 
                                    <input type="file" name="imagen" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar vehiculo</button>
                                </div>
                                <?php if(isset( $_POST["registrar"])){?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Datos ingresados con exito!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>