<?php
if(isset($_GET["idproveedor"])){
date_default_timezone_set("America/Bogota");
$proveedor=new Proveedor($_GET["idproveedor"]);
    if($proveedor -> consultarEstado()==1){
        $proveedor -> inhabilitar();
        $informacion="Se actualizo el estado del Proveedor:<br>identificación: ". $_GET["idproveedor"] . "<br>administrador: ".$_GET["idadministrador"]."<br>Estado anterior: Habilitado<br>Estado posterior: Inhabilitado";
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","4",$_GET["idadministrador"],"",$informacion,$fecha);
        $log -> insertarLogAdministrador();
        echo "<i class='fas fa-times' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></i>";
    }else{
        $proveedor -> habilitar();
        $informacion="Se actualizo el estado del Proveedor:<br>identificación: ". $_GET["idproveedor"] . "<br>administrador: ".$_GET["idadministrador"]."<br>Estado anterior: Inhabilitado<br>Estado posterior: Habilitado";
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","4",$_GET["idadministrador"],"",$informacion,$fecha);
        $log -> insertarLogAdministrador();
        echo "<i class='fas fa-truck' data-toggle='tooltip' data-placement='left' title='Habilitado'></i>";
    }
}
?>