<?php
$registrado=false;
$error=0;
if(isset($_POST["registrar"])){
    $proveedor=new Proveedor();
    if($proveedor -> existeNombre($_POST["nombre"])){
        $error=1;
    }else{
        $proveedor -> registrarNombre($_POST["nombre"]);
        $informacion="Se ingreso el nombre de vehiculo:<br>nombre: ". $_POST["nombre"];
        date_default_timezone_set("America/Bogota");
        $fecha=date('Y-m-j G-i-s');
        $log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
        $log -> insertarLogProveedor();
        $registrado=true;
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body bg-dark mt-1">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <div class="card-header text-white border border-white">
                            <center><h1>Registra el nombre de un vehiculo</h1></center>
                        </div>
                        <div class="card-body text-white border border-white">
                            <form action="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/registrarNombre.php")?>" method="post">
                                    <div class="form-group">
                                        <h4 class="text-white">Nombre</h4>  
                                        <input type="text" name="nombre" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar nombre</button>
                                    </div>
                                    <?php if($registrado){ ?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Datos ingresados con exito!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <?php }else if($error==1){ ?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            Este nombre ya fue ingresado!!
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php } ?>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>