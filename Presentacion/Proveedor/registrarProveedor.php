<?php
$error=0;
$registrado=false;
if(isset($_POST["registrar"])){
    date_default_timezone_set("America/Bogota");
    $nombre=$_POST["nombre"];
    $correo=$_POST["correo"];
    $clave=$_POST["clave"];
    $nit=$_POST["nit"];
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Proveedor/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $proveedor = new Proveedor("",$nombre,$correo,$clave,"",$nit,$rutaRemota);
        if($proveedor -> existeCorreo()){
            $error=1;
        }else{
            $proveedor -> registrarProveedor();
            $proveedor -> autenticar();
            $informacion="Se registro un Proveedor con la siguiente información:<br>identificación: ". $proveedor -> getIdProveedor() ."<br>nombre: ". $nombre ."<br>correo: ". $correo ."<br>nit: ". $nit;
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
            $log -> insertarLogAdministrador();
            $registrado=true;
        }
    }else{
        $proveedor = new Proveedor("",$nombre,$correo,$clave,"",$nit);
        if($proveedor -> existeCorreo()){
            $error=1;
        }else{
            $proveedor -> registrarProveedor();
            $proveedor -> autenticar();
            $informacion="Se registro un Proveedor con la siguiente información:<br>identificación: ". $proveedor -> getIdProveedor() ."<br>nombre: ". $nombre ."<br>correo: ". $correo ."<br>nit: ". $nit;
            $fecha=date('Y-m-j G-i-s');
            $log = new Log("","1",$_SESSION["id"],"",$informacion,$fecha);
            $log -> insertarLogAdministrador();
            $registrado=true;
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card-body text-white bg-dark mt-2">
                <div class="row">
                    <div class="col-md-2 col-0">
                    </div>
                    <div class="col-md-8 col-12">
                        <div class="card-header text-white bg-dark mt-1 text-center border border-white">
                            <h1>Registro de proveedor</h1>
                        </div>
                        <div class="card-body text-white bg-dark mt-1 border border-white">
                            <form action="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/registrarProveedor.php")?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="nombre" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Correo</label>
                                    <input type="email" name="correo" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Clave</label>
                                    <input type="password" name="clave" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Numero de identificación tributaria (NIT)</label>
                                    <input type="text" name="nit" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Imagen</label> 
                                    <input type="file" name="imagen" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar proveedor</button>
                                </div> 
                                <?php if($registrado){?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Datos ingresados con exito!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php }else if($error==1){ ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        Este correo ya fue registrado!!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php } ?>    
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>