<?php
$proveedor= new Proveedor($_SESSION["id"]);
$proveedor -> consultar();
?>
<div class="container-fluid">
  <div class="row">
    <div class="col">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/sesionProveedor.php")?>">
          <img src="Img/iconoP.png" width="30" height="30" class="d-inline-block align-top rounded" alt="" loading="lazy">
          <strong>BoostingCo</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Vehiculos
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/registrarNombre.php") ?>">Registrar nombre</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Proveedor/registrarVehiculo.php") ?>">Registrar vehiculo</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Vehiculo/listaVehiculosProv.php")?>">Lista</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Proveedor: <?php echo $proveedor -> getNombre() ?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Editar perfil</a>
                <a class="dropdown-item" href="#">Editar foto</a>
                <a class="dropdown-item" href="#">Cambiar clave</a>
              </div>
            </li>
            <form class="form-inline" action="index.php?cerrarSesion=1" method="post">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Cerrar sesión</button>
            </form> 
          </ul>
        </div>
      </nav>
    </div>
  </div>
</div>