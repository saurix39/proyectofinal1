<?php
$proveedor= new Proveedor($_SESSION["id"]);
$proveedor -> consultar();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card-header text-white bg-dark mt-2 text-center">
                <h2>Bienvenido a nuestra plataforma</h2>
            </div>
            <div class="card-body text-white bg-dark mt-1">
                <div class="row">
                    <div class="col-3">
                        <img src="<?php echo ($proveedor -> getFoto()!="")?$proveedor -> getFoto():"http://icons.iconarchive.com/icons/elegantthemes/beautiful-flat-one-color/128/loading-icon.png";?>" alt="Imagen del administrador" width="100%">
                    </div>
                    <div class="col-9">
                    <table class="table table-striped table-dark">
                            <tr>
                            <th scope="col">Nombre</th>
                            <td><?php echo ($proveedor -> getNombre()!="")?$proveedor -> getNombre():"Sin completar" ?></td>
                            </tr>
                            <tr>
                            <th scope="col">Correo</th>
                            <td><?php echo ($proveedor -> getCorreo()!="")?$proveedor -> getCorreo():"Sin completar"?></td>
                            </tr>
                            <tr>
                            <th scope="col">Nit</th>
                            <td><?php echo ($proveedor -> getNit()!="")?$proveedor -> getNit():"Sin completar"?></td>
                            </tr>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>